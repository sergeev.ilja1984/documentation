<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd">
<book>

<article id="CALLIGRAPHY">
<articleinfo>
  <title>Calligraphy</title>
  <subtitle>Tutorial</subtitle>
  <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="tutorial-calligraphy-authors.xml" />
</articleinfo>

<abstract>
<para>
One of the many great tools available in Inkscape is the Calligraphy tool.  This tutorial
will help you become acquainted with how that tool works, as well as demonstrate some
basic techniques of the art of Calligraphy.
</para>
<para>
Use <keycombo><keycap function="control">Ctrl</keycap><keycap>arrows</keycap></keycombo>, <mousebutton role="mouse-button">mouse wheel</mousebutton>, or <mousebutton role="middle-button-drag">middle button drag</mousebutton>
to scroll the page down. For basics of object creation, selection, and
transformation, see the Basic tutorial in <menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></menuchoice>.
</para>
</abstract>

<sect1>
<title>History and Styles</title>

<para>
Going by the dictionary definition, <firstterm>calligraphy</firstterm> means “beautiful
writing” or “fair or elegant penmanship”. Essentially, calligraphy is the art of making
beautiful or elegant handwriting. It may sound intimidating, but with a little practice,
anyone can master the basics of this art.
</para>

<para>
The earliest forms of calligraphy date back to cave-man paintings. Up until roughly 1440
AD, before the printing press was around, calligraphy was the way books and other
publications were made. A scribe had to handwrite every individual copy of every book or
publication. The handwriting was done with a quill and ink onto materials such as
parchment or vellum. The lettering styles used throughout the ages include Rustic,
Carolingian, Blackletter, etc. Perhaps the most common place where the average person
will run across calligraphy today is on wedding invitations.
</para>

<para>
There are three main styles of calligraphy:
</para>

<itemizedlist>
<listitem><para>
Western or Roman
</para></listitem> 
<listitem><para>
Arabic 
</para></listitem> 
<listitem><para>
Chinese or Oriental
</para></listitem> 
</itemizedlist>

<para>
This tutorial focuses mainly on Western calligraphy, as the other two styles tend to use
a brush (instead of a pen with nib), which is not how our Calligraphy tool
currently functions.
</para>

<para>
One great advantage that we have over the scribes of the past is the
<guimenuitem>Undo</guimenuitem> command: If you make a mistake, the entire page is not
ruined. Inkscape's Calligraphy tool also enables some techniques which would not be
possible with a traditional pen-and-ink.
</para>
</sect1>

<sect1>
<title>Hardware</title>

<para>
You will get the best results if you use a <firstterm>tablet and pen</firstterm>
(e.g. Wacom). Thanks to the flexibility of our tool, even those with only a mouse can do
some fairly intricate calligraphy, though there will be some difficulty producing fast
sweeping strokes.
</para>

<para>
Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and <firstterm>
tilt sensitivity</firstterm> of a tablet pen that supports these features. The sensitivity functions are disabled by
default because they require configuration. Also, keep in mind that calligraphy with a
quill or pen with nib are also not very sensitive to pressure, unlike a brush.
</para>

<para>
If you have a tablet and would like to utilize the sensitivity features, you will need
to configure your device. This configuration will only need to be performed once and 
the settings are saved. To enable this support you must have the tablet plugged in
prior to starting Inkscape and then proceed to open the <guimenuitem>Input Devices...</guimenuitem> 
dialog through the <guimenu>Edit</guimenu> menu. With this dialog open you can choose the
preferred device and settings for your tablet pen. Lastly, after choosing those settings, switch
to the Calligraphy tool and toggle the toolbar buttons for pressure and tilt. From
now on, Inkscape will remember those settings on startup.
</para>

<para>
The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</firstterm> of
the stroke (see “Thinning” below), so if you are using a mouse, you'll probably want to
zero this parameter.
</para>
</sect1>

<sect1>
<title>Calligraphy Tool Options</title>

<para>
Switch to the Calligraphy tool by pressing <keycombo><keycap function="control">Ctrl</keycap><keycap>F6</keycap></keycombo>, pressing the <keycap>C</keycap> 
key, or by clicking on its toolbar button. On the top toolbar, you will 
notice there are 8 options: <guilabel>Width</guilabel> &amp; <guilabel>Thinning</guilabel>; <guilabel>Angle</guilabel> &amp; <guilabel>Fixation</guilabel>; <guilabel>Caps</guilabel>; <guilabel>Tremor</guilabel>, <guilabel>Wiggle</guilabel> &amp; <guilabel>Mass</guilabel>.
There are also two buttons to toggle tablet Pressure and Tilt sensitivity on and off (for
drawing tablets).
</para>
</sect1>

<sect1>
<title>Width &amp; Thinning</title>

<para>
This pair of options control the <firstterm>width</firstterm> of your pen. The width can
vary from 1 to 100 and (by default) is measured in units relative to the size of
your editing window, but independent of zoom. This makes sense, because the 
natural “unit of measure” in calligraphy is the range of your hand's movement,
and it is therefore convenient to have the width of your pen nib in constant 
ratio to the size of your “drawing board” and not in some real units which 
would make it depend on zoom. This behavior is optional though, so it can be
changed for those who would prefer absolute units regardless of zoom. To switch
to this mode, use the checkbox on the tool's Preferences page (you can open it
by double-clicking the tool button).
</para>

<para>
Since pen width is changed often, you can adjust it without going to 
the toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow keys or with a
tablet that supports the pressure sensitivity function. The best thing about
these keys is that they work while you are drawing, so you can change
the width of your pen gradually in the middle of the stroke:
</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="calligraphy-f01.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>
Pen width may also depend on the velocity, as controlled by the
<firstterm>thinning</firstterm> parameter. This parameter can take values from -100 to 100;
zero means the width is independent of velocity, positive values make faster strokes
thinner, negative values make faster strokes broader. The default of 10 means moderate
thinning of fast strokes. Here are a few examples, all drawn with width=20 and
angle=90:
</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="calligraphy-f02.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>
For fun, set Width and Thinning both to 100 (maximum) and draw with 
jerky movements to get strangely naturalistic, neuron-like shapes: 
</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="calligraphy-f03.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

</sect1>

<sect1>
<title>Angle &amp; Fixation</title>

<para>
After width, <firstterm>angle</firstterm> is the most important calligraphy
parameter. It is the angle of your pen in degrees, changing from 0 (horizontal) to
90 (vertical counterclockwise) or to -90 (vertical clockwise). Note that if you 
turn tilt sensitivity on for a tablet, the angle parameter is greyed out and 
the angle is determined by the tilt of the pen.
</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="calligraphy-f04.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>
Each traditional calligraphy style has its own prevalent pen angle. For example, the
Uncial hand uses the angle of 25 degrees. More complex hands and more experienced
calligraphers will often vary the angle while drawing, and Inkscape makes this possible
by pressing <keycap>up</keycap> and <keycap>down</keycap> arrow keys or with a tablet that
supports the tilt sensitivity feature. For beginning calligraphy lessons, however,
keeping the angle constant will work best. Here are examples of strokes drawn at
different angles (fixation = 100):
</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="calligraphy-f05.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>
As you can see, the stroke is at its thinnest when it is drawn parallel to its angle,
and at its broadest when drawn perpendicular. Positive angles are the most
natural and traditional for right-handed calligraphy.
</para>

<para>
The level of contrast between the thinnest and the thickest is controlled by the
<firstterm>fixation</firstterm> parameter. The value of 100 means that the angle is always
constant, as set in the Angle field. Decreasing fixation lets the pen turn a little
against the direction of the stroke.  With fixation=0, pen rotates freely to be always
perpendicular to the stroke, and Angle has no effect anymore:
</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="calligraphy-f06.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>
Typographically speaking, maximum fixation and therefore maximum stroke width contrast
(above left) are the features of antique serif typefaces, such as Times or Bodoni
(because these typefaces were historically an imitation of fixed-pen calligraphy). Zero
fixation and zero width contrast (above right), on the other hand, suggest modern sans
serif typefaces such as Helvetica.
</para>
</sect1>

<sect1>
<title>Tremor</title>

<para>
<firstterm>Tremor</firstterm> is intended to give a more natural look to the calligraphy strokes.
Tremor is adjustable in the Controls bar with values ranging from 0 to 100. It will 
affect your strokes producing anything from slight unevenness to wild blotches and 
splotches. This significantly expands the creative range of the tool.
</para>
<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="calligraphy-f07.svg"/>
</imageobject>
</mediaobject>
</informalfigure>
</sect1>

<sect1>
<title>Wiggle &amp; Mass</title>

<para>
Unlike width and angle, these two last parameters define how the tool “feels” rather
than affect its visual output. So there won't be any illustrations in this section;
instead just try them yourself to get a better idea.
</para>

<para>
<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement of the
pen. The default is at minimum (0), and increasing this parameter makes paper “slippery”:
if the mass is big, the pen tends to run away on sharp turns; if the mass is zero, high
wiggle makes the pen to wiggle wildly.
</para>

<para>
In physics, <firstterm>mass</firstterm> is what causes inertia; the higher the mass of
the Inkscape calligraphy tool, the more it lags behind your mouse pointer and the more
it smoothes out sharp turns and quick jerks in your stroke. By default this value is
quite small (2) so that the tool is fast and responsive, but you can increase mass to
get slower and smoother pen.
</para>

</sect1>

<sect1>
<title>Calligraphy examples</title>

<para>
Now that you know the basic capabilities of the tool, you can try to produce some real
calligraphy. If you are new to this art, get yourself a good calligraphy book and study
it with Inkscape. This section will show you just a few simple examples.
</para>

<para>
First of all, to do letters, you need to create a pair of rulers to guide you. If you're
going to write in a slanted or cursive hand, add some slanted guides across the two
rulers as well, for example:
</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="calligraphy-f08.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>
Then zoom in so that the height between the rulers corresponds to your most natural hand
movement range, adjust width and angle, and off you go!
</para>

<para>
Probably the first thing you would do as a beginner calligrapher is practice the basic
elements of letters &#8212; vertical and horizontal stems, round strokes, slanted
stems. Here are some letter elements for the Uncial hand:
</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="calligraphy-f09.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>
Several useful tips:
</para>

<itemizedlist>
<listitem><para> 
If your hand is comfortable on the tablet, don't move it. Instead,
scroll the canvas (<keycombo><keycap function="control">Ctrl</keycap><keycap>arrow</keycap></keycombo> keys) with your left hand after finishing
each letter.  </para></listitem>

<listitem><para> 
If your last stroke is bad, just undo it
(<keycombo><keycap function="control">Ctrl</keycap><keycap>Z</keycap></keycombo>). However, if its shape is good but the position or size are
slightly off, it's better to switch to Selector temporarily (<keycap>Space</keycap>) and
nudge/scale/rotate it as needed (using <mousebutton>mouse</mousebutton> or keys), then press <keycap>Space</keycap>
again to return to Calligraphy tool.  
</para></listitem>

<listitem><para> 
Having done a word, switch to Selector again to adjust stem uniformity
and letterspacing. Don't overdo this, however; good calligraphy must retain somewhat
irregular handwritten look.  Resist the temptation to copy over letters and letter
elements; each stroke must be original.  
</para></listitem>
</itemizedlist>

<para>
And here are some complete lettering examples:
</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="calligraphy-f10.svg"/>
</imageobject>
</mediaobject>
</informalfigure>


</sect1>


<sect1>
<title>Conclusion</title>

<para>
Calligraphy is not only fun; it's a deeply spiritual art that may 
transform your outlook on everything you do and see. Inkscape's
calligraphy tool can only serve as a modest introduction. And yet
it is very nice to play with and may be useful in real design. Enjoy! 
</para>
</sect1>
</article>
</book>
