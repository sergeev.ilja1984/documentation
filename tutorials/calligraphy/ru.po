msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Calligraphy\n"
"POT-Creation-Date: 2020-04-28 20:51+0200\n"
"PO-Revision-Date: 2012-01-24 15:50+0300\n"
"Last-Translator: Evgenia Sinichenkova <e.sinichenkova@gmail.com>\n"
"Language-Team: Russian <ru@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: calligraphy-f10.svg:48(format) calligraphy-f09.svg:48(format)
#: calligraphy-f08.svg:48(format) calligraphy-f07.svg:49(format)
#: calligraphy-f06.svg:64(format) calligraphy-f05.svg:48(format)
#: calligraphy-f04.svg:80(format) calligraphy-f03.svg:48(format)
#: calligraphy-f02.svg:48(format) calligraphy-f01.svg:48(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: calligraphy-f10.svg:117(tspan)
#, no-wrap
msgid "Uncial hand"
msgstr "Унциальный шрифт"

#: calligraphy-f10.svg:128(tspan)
#, no-wrap
msgid "Carolingian hand"
msgstr "Каролингский шрифт"

#: calligraphy-f10.svg:139(tspan)
#, no-wrap
msgid "Gothic hand"
msgstr "Готический шрифт"

#: calligraphy-f10.svg:150(tspan)
#, no-wrap
msgid "Bâtarde hand"
msgstr "Рукописный готический шрифт"

#: calligraphy-f10.svg:175(tspan)
#, no-wrap
msgid "Flourished Italic hand"
msgstr "Размашистый готический почерк"

#: calligraphy-f07.svg:73(tspan)
#, no-wrap
msgid "slow"
msgstr "медленно"

#: calligraphy-f07.svg:84(tspan)
#, no-wrap
msgid "medium"
msgstr "средне"

#: calligraphy-f07.svg:95(tspan)
#, no-wrap
msgid "fast"
msgstr "быстро"

#: calligraphy-f07.svg:179(tspan)
#, no-wrap
msgid "tremor = 0"
msgstr "дрожание = 0"

#: calligraphy-f07.svg:190(tspan)
#, no-wrap
msgid "tremor = 10"
msgstr "дрожание = 10"

#: calligraphy-f07.svg:201(tspan)
#, no-wrap
msgid "tremor = 30"
msgstr "дрожание = 30"

#: calligraphy-f07.svg:212(tspan)
#, no-wrap
msgid "tremor = 50"
msgstr "дрожание = 50"

#: calligraphy-f07.svg:223(tspan)
#, no-wrap
msgid "tremor = 70"
msgstr "дрожание = 70"

#: calligraphy-f07.svg:234(tspan)
#, no-wrap
msgid "tremor = 90"
msgstr "дрожание = 90"

#: calligraphy-f07.svg:245(tspan)
#, no-wrap
msgid "tremor = 20"
msgstr "дрожание = 20"

#: calligraphy-f07.svg:256(tspan)
#, no-wrap
msgid "tremor = 40"
msgstr "дрожание = 40"

#: calligraphy-f07.svg:267(tspan)
#, no-wrap
msgid "tremor = 60"
msgstr "дрожание = 60"

#: calligraphy-f07.svg:278(tspan)
#, no-wrap
msgid "tremor = 80"
msgstr "дрожание = 80"

#: calligraphy-f07.svg:289(tspan)
#, no-wrap
msgid "tremor = 100"
msgstr "дрожание = 100"

#: calligraphy-f06.svg:85(tspan) calligraphy-f06.svg:101(tspan)
#: calligraphy-f06.svg:117(tspan) calligraphy-f05.svg:69(tspan)
#, no-wrap
msgid "angle = 30"
msgstr "угол = 30"

#: calligraphy-f06.svg:90(tspan)
#, no-wrap
msgid "fixation = 100"
msgstr "фиксация = 100"

#: calligraphy-f06.svg:106(tspan)
#, no-wrap
msgid "fixation = 80"
msgstr "фиксация = 80"

#: calligraphy-f06.svg:122(tspan)
#, no-wrap
msgid "fixation = 0"
msgstr "фиксация = 0"

#: calligraphy-f05.svg:80(tspan)
#, no-wrap
msgid "angle = 60"
msgstr "угол = 60"

#: calligraphy-f05.svg:91(tspan)
#, no-wrap
msgid "angle = 90"
msgstr "угол = 90"

#: calligraphy-f05.svg:102(tspan) calligraphy-f04.svg:142(tspan)
#, no-wrap
msgid "angle = 0"
msgstr "угол = 0"

#: calligraphy-f05.svg:113(tspan)
#, no-wrap
msgid "angle = 15"
msgstr "угол = 15"

#: calligraphy-f05.svg:124(tspan)
#, fuzzy, no-wrap
msgid "angle = −45"
msgstr "угол = -45"

#: calligraphy-f04.svg:120(tspan)
#, no-wrap
msgid "angle = 90 deg"
msgstr "угол = 90"

#: calligraphy-f04.svg:131(tspan)
#, no-wrap
msgid "angle = 30 (default)"
msgstr "угол = 30 (по умолчанию)"

#: calligraphy-f04.svg:153(tspan)
#, fuzzy, no-wrap
msgid "angle = −90 deg"
msgstr "угол = 90"

#: calligraphy-f02.svg:69(tspan)
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "сужение = 0 (одинаковая ширина)"

#: calligraphy-f02.svg:80(tspan)
#, no-wrap
msgid "thinning = 10"
msgstr "сужение = 10"

#: calligraphy-f02.svg:91(tspan)
#, no-wrap
msgid "thinning = 40"
msgstr "сужение = 40"

#: calligraphy-f02.svg:102(tspan)
#, fuzzy, no-wrap
msgid "thinning = −20"
msgstr "сужение = -20"

#: calligraphy-f02.svg:113(tspan)
#, fuzzy, no-wrap
msgid "thinning = −60"
msgstr "сужение = -60"

#: calligraphy-f01.svg:69(tspan)
#, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "ширина=1, увеличивается...                       достигает 47, уменьшается...                                  снова 0"

#: tutorial-calligraphy.xml:7(title)
msgid "Calligraphy"
msgstr "Каллиграфия"

#: tutorial-calligraphy.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-calligraphy.xml:13(para)
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr ""
"Один из замечательных инструментов Inkscape — каллиграфическое перо. Этот "
"урок поможет вам лучше узнать возможности этого пера и расскажет о некоторых "
"стандартных приёмах каллиграфии."

#: tutorial-calligraphy.xml:18(para)
#, fuzzy
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-button\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""
"Используйте <keycap>Ctrl+стрелки</keycap>, <keycap>колесо мыши</keycap> или "
"<keycap>перемещение с нажатой средней клавишей мыши</keycap> для просмотра "
"текста. Для получения начальных знаний о создании объектов их выделении и "
"изменении их форм, смотрите урок «Основы» в меню <command>Справка &gt; "
"Учебник</command>."

#: tutorial-calligraphy.xml:26(title)
msgid "History and Styles"
msgstr "История и стили"

#: tutorial-calligraphy.xml:28(para)
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, "
"calligraphy is the art of making beautiful or elegant handwriting. It may "
"sound intimidating, but with a little practice, anyone can master the basics "
"of this art."
msgstr ""
"Следуя словарному определению, <firstterm>каллиграфия</firstterm> означает "
"«красивое письмо» или «искусство красивого и чёткого письма». То есть "
"каллиграфия — это искусство красивого написания. Возможно, это "
"представляется очень сложным делом, но с небольшой практикой любой сможет "
"освоить основы каллиграфии."

#: tutorial-calligraphy.xml:35(para)
msgid ""
"The earliest forms of calligraphy date back to cave-man paintings. Up until "
"roughly 1440 AD, before the printing press was around, calligraphy was the "
"way books and other publications were made. A scribe had to handwrite every "
"individual copy of every book or publication. The handwriting was done with "
"a quill and ink onto materials such as parchment or vellum. The lettering "
"styles used throughout the ages include Rustic, Carolingian, Blackletter, "
"etc. Perhaps the most common place where the average person will run across "
"calligraphy today is on wedding invitations."
msgstr ""
"Ранние формы каллиграфии уходят корнями к рисункам древних людей. До 1440 "
"года, года изобретения печатного пресса, каллиграфия была единственным "
"способом создания книг и других публикаций. Писцу приходилось переписывать "
"вручную каждую копию книги или издания. На пергаменте или кальке писали при "
"помощи птичьего пера и чернил. Школы письма существовали во все времена. "
"Сейчас же каллиграфическое письмо можно чаще встретить на свадебных "
"приглашениях."

#: tutorial-calligraphy.xml:45(para)
msgid "There are three main styles of calligraphy:"
msgstr "Существуют три основных вида каллиграфии:"

#: tutorial-calligraphy.xml:50(para)
msgid "Western or Roman"
msgstr "Западный или Романский"

#: tutorial-calligraphy.xml:53(para)
msgid "Arabic"
msgstr "Классическое арабское письмо"

#: tutorial-calligraphy.xml:56(para)
msgid "Chinese or Oriental"
msgstr "Китайский или Восточно-азиатский"

#: tutorial-calligraphy.xml:61(para)
msgid ""
"This tutorial focuses mainly on Western calligraphy, as the other two styles "
"tend to use a brush (instead of a pen with nib), which is not how our "
"Calligraphy tool currently functions."
msgstr ""
"Этот учебник в основном уделяет внимание западной каллиграфии, так как "
"другие два стиля используют кисточку (вместо острого пера), которая пишет не "
"так как наше каллиграфическое перо."

#: tutorial-calligraphy.xml:67(para)
#, fuzzy
msgid ""
"One great advantage that we have over the scribes of the past is the "
"<guimenuitem>Undo</guimenuitem> command: If you make a mistake, the entire "
"page is not ruined. Inkscape's Calligraphy tool also enables some techniques "
"which would not be possible with a traditional pen-and-ink."
msgstr ""
"У нас есть одно огромное преимущество перед писцами прошлого — команда "
"<command>Отменить</command>. В случае ошибки мы не погубим всю страницу. К "
"тому же, инструмент каллиграфии в Inkscape позволяет использовать такие "
"художественные приёмы, которые были бы невозможны с обычным пером и "
"чернилами."

#: tutorial-calligraphy.xml:76(title)
msgid "Hardware"
msgstr "Технические средства"

#: tutorial-calligraphy.xml:78(para)
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm> (e.g. Wacom). Thanks to the flexibility of our tool, even those "
"with only a mouse can do some fairly intricate calligraphy, though there "
"will be some difficulty producing fast sweeping strokes."
msgstr ""
"Вы получите наилучшие результаты, используя <firstterm>планшет и перо</"
"firstterm> (например Wacom), но при помощи мыши также можно получить "
"неплохой результат. Правда, будет проблематично получить сложно изогнутые "
"линии так же качественно и быстро как с планшетом."

#: tutorial-calligraphy.xml:85(para)
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr ""
"Inkscape умеет интерпретировать <firstterm>силу нажатия</firstterm> и "
"<firstterm>угол наклона</firstterm> пера относительно плоскости планшета. "
"Эти функции по умолчанию отключены, поскольку требуют вдумчивой "
"индивидуальной настройки. Помните, что каллиграфия при помощи птичьего пера "
"или перьевой ручки в отличие от кисти не слишком чувствительна к наклону."

#: tutorial-calligraphy.xml:92(para)
#, fuzzy
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <guimenuitem>Input Devices...</guimenuitem> dialog through the "
"<guimenu>Edit</guimenu> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar "
"buttons for pressure and tilt. From now on, Inkscape will remember those "
"settings on startup."
msgstr ""
"Если у вас есть планшет, и вы хотели бы использовать упомянутые выше "
"возможности, вам придётся настроить его для работы. Эта настройка делается "
"лишь раз. Для начала необходимо подключить сам планшет перед запуском "
"Inkscape, а затем из меню <emphasis>Файл</emphasis> открыть диалог "
"<firstterm>Устройства ввода...</firstterm>. В этом диалоге выбирается "
"предпочитаемое устройство и настройки пера планшета. После этого "
"переключитесь на каллиграфическое перо и в панели параметров включите "
"распознавание силы нажатия и угла. Теперь Inkscape будет загружать эти "
"настройки при каждом запуске."

#: tutorial-calligraphy.xml:103(para)
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a "
"mouse, you'll probably want to zero this parameter."
msgstr ""
"Каллиграфическое перо в Inkscape может быть чувствительным к "
"<firstterm>скорости</firstterm> написания (см. далее «Сужение»), так что "
"если вы используете мышь, то, вероятно, захотите обнулить этот параметр."

#: tutorial-calligraphy.xml:111(title)
msgid "Calligraphy Tool Options"
msgstr "Параметры инструмента каллиграфии"

#: tutorial-calligraphy.xml:113(para)
#, fuzzy
msgid ""
"Switch to the Calligraphy tool by pressing <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo>, pressing the "
"<keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: <guilabel>Width</guilabel> "
"&amp; <guilabel>Thinning</guilabel>; <guilabel>Angle</guilabel> &amp; "
"<guilabel>Fixation</guilabel>; <guilabel>Caps</guilabel>; <guilabel>Tremor</"
"guilabel>, <guilabel>Wiggle</guilabel> &amp; <guilabel>Mass</guilabel>. "
"There are also two buttons to toggle tablet Pressure and Tilt sensitivity on "
"and off (for drawing tablets)."
msgstr ""
"Переключитесь на инструмент каллиграфии, нажав <keycap>Ctrl+F6</keycap>, "
"клавишу <keycap>C</keycap> или нажав кнопку Рисовать каллиграфическим пером "
"на Панели инструментов. На верхней панели расположены 8 параметров: Ширина; "
"Сужение; Угол; Фиксация; Дрожание штриха; Масса пера; Концы и Виляние пером. "
"Здесь же расположены кнопки для переключения: Нажим (pressure) и Толщина "
"линии пера."

#: tutorial-calligraphy.xml:123(title)
msgid "Width &amp; Thinning"
msgstr "Ширина и Сужение"

#: tutorial-calligraphy.xml:125(para)
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom. This "
"makes sense, because the natural “unit of measure” in calligraphy is the "
"range of your hand's movement, and it is therefore convenient to have the "
"width of your pen nib in constant ratio to the size of your “drawing board” "
"and not in some real units which would make it depend on zoom. This behavior "
"is optional though, so it can be changed for those who would prefer absolute "
"units regardless of zoom. To switch to this mode, use the checkbox on the "
"tool's Preferences page (you can open it by double-clicking the tool button)."
msgstr ""
"Эта пара настроек отвечает за <firstterm>ширину</firstterm> вашего пера. "
"Значение ширины может составлять от 1 до 100 и измеряется в единицах "
"относительно размера окна программы, но независимо от масштаба. Это важно, "
"поскольку настоящая «единица измерения» в каллиграфии — это диапазон "
"движения вашей руки, и, следовательно, удобно, когда ширина кончика пера "
"является константой по отношению к размеру вашего «холста», а не какими-"
"нибудь настоящими единицами, зависящими от масштаба. Тем не менее, это "
"поведение опционально и может быть изменено, если вы предпочитаете "
"абсолютные единицы, не зависящие от масштаба. Для переключения типа "
"поведения используйте переключатель на странице инструмента Каллиграфическое "
"перо в диалоге настройки программы, которую можно открыть двойным щелчком по "
"значку инструмента."

#: tutorial-calligraphy.xml:138(para)
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr ""
"Так как ширина пера часто меняется, вы можете менять её не только в панели "
"настроек инструмента, но и при помощи <keycap>левой</keycap> и "
"<keycap>правой</keycap> стрелок клавиатуры, либо с помощью пера планшета, "
"поддерживающего распознавание силы нажатия. Главное качество этой функции "
"состоит в том, что она работает в момент рисования, поэтому вы можете менять "
"ширину пера постепенно по ходу действия:"

#: tutorial-calligraphy.xml:153(para)
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr ""
"Ширина пера также может зависеть от скорости письма, что контролируется "
"параметром <firstterm>Сужение</firstterm>. Значение этого параметра может "
"быть от -100 до 100; ноль означает, что ширина не зависит от скорости "
"написания, положительное значение сужает быстро начерченные линии, "
"отрицательное — расширяет. Начальное значение 10 придаёт линиям умеренное "
"сужение при быстром письме. Ниже приведены некоторые примеры изложенного. "
"Все штрихи нарисованы со значением ширины=20 и углом=90:"

#: tutorial-calligraphy.xml:169(para)
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr ""
"Для развлечения установите значения ширины и сужения равным 100 "
"(максимальное) и порисуйте резкими движениями. Получаются странные фигуры, "
"похожие на нейроны:"

#: tutorial-calligraphy.xml:184(title)
msgid "Angle &amp; Fixation"
msgstr "Угол и Фиксация"

#: tutorial-calligraphy.xml:186(para)
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the "
"angle parameter is greyed out and the angle is determined by the tilt of the "
"pen."
msgstr ""
"Наиболее важный каллиграфический параметр после ширины — это "
"<firstterm>угол</firstterm> вашего пера в градусах. Он меняется от 0 "
"(горизонтальное положение) до 90 (вертикальное положение, против часовой "
"стрелки) или -90 (вертикальное по часовой):"

#: tutorial-calligraphy.xml:201(para)
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands "
"and more experienced calligraphers will often vary the angle while drawing, "
"and Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr ""
"Каждый традиционный каллиграфический стиль имеет свой превалирующий угол "
"пера. Например, унцианский шрифт использует угол в 25 градусов. Более "
"сложные стили и более опытные каллиграфы часто меняют угол во время работы, "
"и Inkscape позволяет делать это с помощью клавиатурных стрелок "
"<keycap>вверх</keycap> и <keycap>вниз</keycap> . Для начала каллиграфических "
"занятий предустановленное значение прекрасно подойдёт. На примерах ниже, "
"штрихи имеют разный угол (фиксация = 100):"

#: tutorial-calligraphy.xml:218(para)
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr ""
"Как видно на примере, штрих тоньше, когда идёт параллельно своему углу, и "
"шире, когда идёт перпендикулярно ему. Положительные значения параметра Угол "
"соответствуют более традиционной каллиграфии для правой руки."

#: tutorial-calligraphy.xml:224(para)
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr ""
"Скорость перехода от толстого к тонкому контролируется параметром "
"<firstterm>Фиксация</firstterm>. Значение равное единице означает что угол "
"постоянен и равен значению установленному параметром Угол. Уменьшение "
"фиксации позволит перу немного повернуться против движения штриха. Со "
"значением фиксации 0 перо поворачивается свободно, пытаясь быть "
"перпендикулярным движению, и значение параметра Угол не имеет никакого "
"эффекта:"

#: tutorial-calligraphy.xml:239(para)
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke "
"width contrast (above left) are the features of antique serif typefaces, "
"such as Times or Bodoni (because these typefaces were historically an "
"imitation of fixed-pen calligraphy). Zero fixation and zero width contrast "
"(above right), on the other hand, suggest modern sans serif typefaces such "
"as Helvetica."
msgstr ""
"Говоря языком типографов, максимальная фиксация, а следовательно "
"максимальная ширина (верхний левый пример) — характерная черта старинных "
"шрифтов, таких как Times или Bodoni (исторически сложилось что эти шрифты "
"копируют каллиграфию фиксированного пера). С другой стороны, нулевую "
"фиксация и нулевую ширину (верхний правый пример) мы можем увидеть в "
"современных шрифтах без засечек, таких как Helvetica."

#: tutorial-calligraphy.xml:249(title)
msgid "Tremor"
msgstr "Дрожание"

#: tutorial-calligraphy.xml:251(para)
#, fuzzy
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr ""
"<firstterm>Дрожание</firstterm> необходимо для придания каллиграфическим "
"штрихам более реалистичного вида. Значение дрожания изменяется в панели "
"настроек инструмента в диапазоне между 0,0 и 1,0. С его помощью можно "
"получить как слегка неровные линии, так и совершенно съехавшие с катушек "
"штрихи. Это существенно увеличивает творческий потенциал инструмента."

#: tutorial-calligraphy.xml:266(title)
msgid "Wiggle &amp; Mass"
msgstr "Масса и Торможение"

#: tutorial-calligraphy.xml:268(para)
msgid ""
"Unlike width and angle, these two last parameters define how the tool "
"“feels” rather than affect its visual output. So there won't be any "
"illustrations in this section; instead just try them yourself to get a "
"better idea."
msgstr ""
"В отличие от ширины и угла, эти два параметра определяют «чувствительность» "
"инструмента, а не внешний вид штрихов. Так что в этом разделе не будет "
"никаких примеров. Просто попробуйте рисовать, меняя значения, чтобы понять "
"смысл этих настроек."

#: tutorial-calligraphy.xml:274(para)
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr ""
"<firstterm>Торможение</firstterm> — это сопротивление бумаги движению пера. "
"Изначально установлено максимальное значение (0), и уменьшение этого "
"параметра делает бумагу «скользкой». Если масса большая, перо уходит в "
"сторону при резких поворотах, а если масса нулевая, то от коротких движений "
"перо дико ёрзает."

#: tutorial-calligraphy.xml:281(para)
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your "
"mouse pointer and the more it smoothes out sharp turns and quick jerks in "
"your stroke. By default this value is quite small (2) so that the tool is "
"fast and responsive, but you can increase mass to get slower and smoother "
"pen."
msgstr ""
"В физике <firstterm>масса</firstterm> — причина инерции. Чем больше значение "
"массы, тем больше задержка между движением вашей мыши и тем больше "
"смягчаются резкие повороты штриха. Изначально это значение очень мало (2), "
"поэтому инструмент реагирует быстро. Но массу можно увеличить для получения "
"замедленного пера и гладких линий."

#: tutorial-calligraphy.xml:292(title)
msgid "Calligraphy examples"
msgstr "Примеры каллиграфии"

#: tutorial-calligraphy.xml:294(para)
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr ""
"Ознакомившись с возможностями инструмента, можно начать практиковаться в "
"настоящей каллиграфии. Если вы новичок в этом деле, найдите хорошую книгу по "
"каллиграфии (например, «Творческая каллиграфия» Малькольма Кауча) и "
"занимайтесь по ней, используя Inkscape. В этом разделе приведено несколько "
"простых примеров."

#: tutorial-calligraphy.xml:300(para)
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr ""
"Для того чтобы начать писать буквы, нужно усвоить некоторые правила. Если вы "
"собрались писать наклонным или рукописным шрифтом, то удобно использовать "
"направляющие линейки (как в школьных тетрадках):"

#: tutorial-calligraphy.xml:313(para)
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr ""
"Потом следует подобрать масштаб так, чтобы расстояние между линейками "
"соответствовало вашему обычному размаху. Настройте ширину, угол — и вперёд!"

#: tutorial-calligraphy.xml:318(para)
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round "
"strokes, slanted stems. Here are some letter elements for the Uncial hand:"
msgstr ""
"Вероятно, первое, что вам захочется сделать как начинающему писцу — это "
"попрактиковаться в написании элементов букв — вертикальных и горизонтальных "
"линий, круглых и наклонных штрихов. Ниже изображены некоторые элементы букв "
"унциального (Unicial) шрифта:"

#: tutorial-calligraphy.xml:331(para)
msgid "Several useful tips:"
msgstr "Несколько полезных советов:"

#: tutorial-calligraphy.xml:336(para)
#, fuzzy
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll "
"the canvas (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>arrow</keycap></keycombo> keys) with your left hand after "
"finishing each letter."
msgstr ""
"Если ваша рука удобно лежит на планшете, двигайте холст не ей, а другой "
"рукой <keycap>Ctrl+стрелки</keycap>."

#: tutorial-calligraphy.xml:341(para)
#, fuzzy
msgid ""
"If your last stroke is bad, just undo it (<keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>). However, if its "
"shape is good but the position or size are slightly off, it's better to "
"switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using <mousebutton>mouse</mousebutton> or keys), then "
"press <keycap>Space</keycap> again to return to Calligraphy tool."
msgstr ""
"Если штрих получился неудачным, просто избавьтесь от него (<keycap>Ctrl+Z</"
"keycap>). Если же фигура хороша, но немного не к месту или размер не тот, "
"проще временно переключиться на инструмент выделения (<keycap>Space</"
"keycap>) и подправить размер/форму/местоположение по необходимости "
"(используя мышь и клавиши клавиатуры), а затем снова вернуться к "
"каллиграфическому перу (ещё раз нажав <keycap>Space</keycap>)."

#: tutorial-calligraphy.xml:349(para)
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr ""
"Закончив с написанием слова, снова включите инструмент выделения, чтобы "
"избавиться от неточностей и неровностей, но не перестарайтесь: хорошая "
"каллиграфия должна быть не совсем точной, чтобы быть похожей на рукописную. "
"Удерживайте себя от искушения копировать буквы и элементы букв: каждая линия "
"должна быть оригинальной."

#: tutorial-calligraphy.xml:357(para)
msgid "And here are some complete lettering examples:"
msgstr "Ниже приведены готовые примеры:"

#: tutorial-calligraphy.xml:373(title)
msgid "Conclusion"
msgstr "Заключение"

#: tutorial-calligraphy.xml:375(para)
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with "
"and may be useful in real design. Enjoy!"
msgstr ""
"Каллиграфия — не просто забава, это возвышенное искусство. Она может "
"изменить ваш взгляд на всё, что вы делаете и видите. Инструмент каллиграфии "
"в Inkscape предлагает только скромное введение в увлекательный мир "
"каллиграфии. И всё же, с этим пером приятно работать и оно более чем "
"пригодно для серьёзных дизайнерских работ. Рисуйте и получайте удовольствие!"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-calligraphy.xml:0(None)
msgid "translator-credits"
msgstr ""
"Yura aka Xxaxx <Zhiz0id@gmail.com>, 2005\n"
"Alexandre Prokoudine <alexandre.prokoudine@gmail.com>, 2008, 2009\n"
"Evgenia Sinichenkova <e.sinichenkova@gmail.com>, 2012"

#~ msgid "angle = -90 deg"
#~ msgstr "угол = -90"

#~ msgid ""
#~ "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
#~ msgstr ""
#~ "bulia byak, buliabyak@users.sf.net и josh andler, scislac@users.sf.net"
