msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Calligraphy\n"
"POT-Creation-Date: 2020-04-28 20:51+0200\n"
"PO-Revision-Date: 2012-03-20 19:45+0900\n"
"Last-Translator: Masato Hashimoto <cabezon.hashimoto@gmail.com>\n"
"Language-Team: Japanese <inkscape-translator@lists.sourceforge.net>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Japanese\n"

#: calligraphy-f10.svg:48(format) calligraphy-f09.svg:48(format)
#: calligraphy-f08.svg:48(format) calligraphy-f07.svg:49(format)
#: calligraphy-f06.svg:64(format) calligraphy-f05.svg:48(format)
#: calligraphy-f04.svg:80(format) calligraphy-f03.svg:48(format)
#: calligraphy-f02.svg:48(format) calligraphy-f01.svg:48(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: calligraphy-f10.svg:117(tspan)
#, no-wrap
msgid "Uncial hand"
msgstr "アンシャル体"

#: calligraphy-f10.svg:128(tspan)
#, no-wrap
msgid "Carolingian hand"
msgstr "カロリング体"

#: calligraphy-f10.svg:139(tspan)
#, no-wrap
msgid "Gothic hand"
msgstr "ゴシック体"

#: calligraphy-f10.svg:150(tspan)
#, no-wrap
msgid "Bâtarde hand"
msgstr "バタード体"

#: calligraphy-f10.svg:175(tspan)
#, no-wrap
msgid "Flourished Italic hand"
msgstr "フラリッシュイタリック体"

#: calligraphy-f07.svg:73(tspan)
#, no-wrap
msgid "slow"
msgstr "ゆっくり"

#: calligraphy-f07.svg:84(tspan)
#, no-wrap
msgid "medium"
msgstr "普通"

#: calligraphy-f07.svg:95(tspan)
#, no-wrap
msgid "fast"
msgstr "速く"

#: calligraphy-f07.svg:179(tspan)
#, no-wrap
msgid "tremor = 0"
msgstr "震え = 0"

#: calligraphy-f07.svg:190(tspan)
#, no-wrap
msgid "tremor = 10"
msgstr "震え = 10"

#: calligraphy-f07.svg:201(tspan)
#, no-wrap
msgid "tremor = 30"
msgstr "震え = 30"

#: calligraphy-f07.svg:212(tspan)
#, no-wrap
msgid "tremor = 50"
msgstr "震え = 50"

#: calligraphy-f07.svg:223(tspan)
#, no-wrap
msgid "tremor = 70"
msgstr "震え = 70"

#: calligraphy-f07.svg:234(tspan)
#, no-wrap
msgid "tremor = 90"
msgstr "震え = 90"

#: calligraphy-f07.svg:245(tspan)
#, no-wrap
msgid "tremor = 20"
msgstr "震え = 20"

#: calligraphy-f07.svg:256(tspan)
#, no-wrap
msgid "tremor = 40"
msgstr "震え = 40"

#: calligraphy-f07.svg:267(tspan)
#, no-wrap
msgid "tremor = 60"
msgstr "震え = 60"

#: calligraphy-f07.svg:278(tspan)
#, no-wrap
msgid "tremor = 80"
msgstr "震え = 80"

#: calligraphy-f07.svg:289(tspan)
#, no-wrap
msgid "tremor = 100"
msgstr "震え = 100"

#: calligraphy-f06.svg:85(tspan) calligraphy-f06.svg:101(tspan)
#: calligraphy-f06.svg:117(tspan) calligraphy-f05.svg:69(tspan)
#, no-wrap
msgid "angle = 30"
msgstr "角度 = 30"

#: calligraphy-f06.svg:90(tspan)
#, no-wrap
msgid "fixation = 100"
msgstr "固定度 = 100"

#: calligraphy-f06.svg:106(tspan)
#, no-wrap
msgid "fixation = 80"
msgstr "固定度 = 80"

#: calligraphy-f06.svg:122(tspan)
#, no-wrap
msgid "fixation = 0"
msgstr "固定度 = 0"

#: calligraphy-f05.svg:80(tspan)
#, no-wrap
msgid "angle = 60"
msgstr "角度 = 60"

#: calligraphy-f05.svg:91(tspan)
#, no-wrap
msgid "angle = 90"
msgstr "角度 = 90"

#: calligraphy-f05.svg:102(tspan) calligraphy-f04.svg:142(tspan)
#, no-wrap
msgid "angle = 0"
msgstr "角度 = 0"

#: calligraphy-f05.svg:113(tspan)
#, no-wrap
msgid "angle = 15"
msgstr "角度 = 15"

#: calligraphy-f05.svg:124(tspan)
#, fuzzy, no-wrap
msgid "angle = −45"
msgstr "角度 = -45"

#: calligraphy-f04.svg:120(tspan)
#, no-wrap
msgid "angle = 90 deg"
msgstr "角度 = 90°"

#: calligraphy-f04.svg:131(tspan)
#, no-wrap
msgid "angle = 30 (default)"
msgstr "角度 = 30 (デフォルト)"

#: calligraphy-f04.svg:153(tspan)
#, fuzzy, no-wrap
msgid "angle = −90 deg"
msgstr "角度 = 90°"

#: calligraphy-f02.svg:69(tspan)
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "幅変化 = 0 (一定幅) "

#: calligraphy-f02.svg:80(tspan)
#, no-wrap
msgid "thinning = 10"
msgstr "幅変化 = 10"

#: calligraphy-f02.svg:91(tspan)
#, no-wrap
msgid "thinning = 40"
msgstr "幅変化 = 40"

#: calligraphy-f02.svg:102(tspan)
#, fuzzy, no-wrap
msgid "thinning = −20"
msgstr "幅変化 = -20"

#: calligraphy-f02.svg:113(tspan)
#, fuzzy, no-wrap
msgid "thinning = −60"
msgstr "幅変化 = -60"

#: calligraphy-f01.svg:69(tspan)
#, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "幅=1, 増加....                                      幅=47, 減少...                                               幅=0 に戻る"

#: tutorial-calligraphy.xml:7(title)
msgid "Calligraphy"
msgstr "カリグラフィ"

#: tutorial-calligraphy.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-calligraphy.xml:13(para)
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr ""
"Inkscape で使えるもっとも素晴らしいツールの一つがカリグラフィです。このチュー"
"トリアルではカリグラフィがどのように動くかについて説明し、カリグラフィアート"
"の基本的なテクニックを紹介します。"

#: tutorial-calligraphy.xml:18(para)
#, fuzzy
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-button\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""
"<keycap>Ctrl+矢印</keycap>、<keycap>マウスホイール</keycap>、または <keycap>"
"中央ボタンを押しながらドラッグ</keycap> を使ってページをスクロールすることが"
"できます。基本的なオブジェクトの作成、選択、変形については、 <command>ヘルプ "
"&gt; チュートリアル</command> から基本チュートリアルをご覧ください。"

#: tutorial-calligraphy.xml:26(title)
msgid "History and Styles"
msgstr "歴史と様式"

#: tutorial-calligraphy.xml:28(para)
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, "
"calligraphy is the art of making beautiful or elegant handwriting. It may "
"sound intimidating, but with a little practice, anyone can master the basics "
"of this art."
msgstr ""
"辞書によると、<firstterm>カリグラフィ</firstterm> とは「美しい文字」あるいは"
"「美しく気品ある書法」を意味します。本来カリグラフィとは、美しい、あるいは気"
"品ある手書き筆跡を作成する芸術のことです。それは少々堅苦しそうな響きに聞こえ"
"るかもしれませんが、少しの練習で誰でもこの芸術の基本を習得することができま"
"す。"

#: tutorial-calligraphy.xml:35(para)
msgid ""
"The earliest forms of calligraphy date back to cave-man paintings. Up until "
"roughly 1440 AD, before the printing press was around, calligraphy was the "
"way books and other publications were made. A scribe had to handwrite every "
"individual copy of every book or publication. The handwriting was done with "
"a quill and ink onto materials such as parchment or vellum. The lettering "
"styles used throughout the ages include Rustic, Carolingian, Blackletter, "
"etc. Perhaps the most common place where the average person will run across "
"calligraphy today is on wedding invitations."
msgstr ""
"最古のカリグラフィといえば洞窟画にまで遡ります。印刷機が登場する以前、おおよ"
"そ紀元 1440 年頃まで、カリグラフィは書籍やその他の出版物を作成する手段でし"
"た。写字生はすべての書籍あるいは出版物の、すべての複製をそれぞれ手書きで作成"
"しなければなりませんでした。手書きは羊皮紙やベラムの上に羽ペンとインクを使用"
"して行われました。時代を経て使用されている書体には、ラスティック体、カロリン"
"グ体、ブラックレター体などがあります。おそらく、こんにち一般の人々がカリグラ"
"フィを目にする最も一般的なものと言えば結婚式の招待状でしょうか。"

#: tutorial-calligraphy.xml:45(para)
msgid "There are three main styles of calligraphy:"
msgstr "カリグラフィには、主に 3 つのスタイルがあります:"

#: tutorial-calligraphy.xml:50(para)
msgid "Western or Roman"
msgstr "西洋またはローマ風"

#: tutorial-calligraphy.xml:53(para)
msgid "Arabic"
msgstr "アラビア風"

#: tutorial-calligraphy.xml:56(para)
msgid "Chinese or Oriental"
msgstr "中国または東洋風"

#: tutorial-calligraphy.xml:61(para)
msgid ""
"This tutorial focuses mainly on Western calligraphy, as the other two styles "
"tend to use a brush (instead of a pen with nib), which is not how our "
"Calligraphy tool currently functions."
msgstr ""
"このチュートリアルでは主に西洋風カリグラフィに焦点を当てています。他の 2 つに"
"ついては、現在のカリグラフィツールの機能よりも、どちらかといえば (ペンではな"
"く) ブラシを使うべきでしょう。"

#: tutorial-calligraphy.xml:67(para)
#, fuzzy
msgid ""
"One great advantage that we have over the scribes of the past is the "
"<guimenuitem>Undo</guimenuitem> command: If you make a mistake, the entire "
"page is not ruined. Inkscape's Calligraphy tool also enables some techniques "
"which would not be possible with a traditional pen-and-ink."
msgstr ""
"過去の写字生たちに対し私たちがもつ圧倒的なアドバンテージが <command>元に戻す"
"</command> コマンドの存在です。もし書き損じたとしてもそのページが台無しになっ"
"たりしません。Inkscape のカリグラフィツールはまた、古典的なペンとインクでは不"
"可能なテクニックも使えます。"

#: tutorial-calligraphy.xml:76(title)
msgid "Hardware"
msgstr "ハードウェア"

#: tutorial-calligraphy.xml:78(para)
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm> (e.g. Wacom). Thanks to the flexibility of our tool, even those "
"with only a mouse can do some fairly intricate calligraphy, though there "
"will be some difficulty producing fast sweeping strokes."
msgstr ""
"たとえば Wacom などの <firstterm>ペンタブレット</firstterm> を使えば最もよい"
"結果が得られるでしょう。このツールには柔軟性があるので、マウスだけでも、素早"
"く曲線を引くことには若干の困難がありますが、かなり複雑なカリグラフィを書くこ"
"とができます。"

#: tutorial-calligraphy.xml:85(para)
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr ""
"Inkscape はタブレットペンがサポートする <firstterm>筆圧検知</firstterm> や "
"<firstterm>傾き検知</firstterm> 機能に対応しています。設定が必要であるため、"
"検知機能はデフォルトでは無効になっています。また、ペンでのカリグラフィは、ブ"
"ラシとは異なり筆圧検知に敏感ではないことも覚えておいてください。"

#: tutorial-calligraphy.xml:92(para)
#, fuzzy
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <guimenuitem>Input Devices...</guimenuitem> dialog through the "
"<guimenu>Edit</guimenu> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar "
"buttons for pressure and tilt. From now on, Inkscape will remember those "
"settings on startup."
msgstr ""
"タブレットで検知機能を使いたい場合はデバイスの設定が必要になります。設定は一"
"度行えばそれが保存されます。この機能を有効にするには、まず Inkscape を起動す"
"る前にタブレットを接続しておいてください。起動後、<emphasis>ファイル</"
"emphasis> メニューの <firstterm>入力デバイス...</firstterm> コマンドを選択し"
"てください。ダイアログが表示され、既定のデバイスおよびタブレットペンの設定が"
"行えます。設定を保存したら、カリグラフィツールに切り替え、ツールバー上の筆圧"
"および傾き検知ボタンを押してください。以降、Inkscape は起動時にそれら設定を復"
"元します。"

#: tutorial-calligraphy.xml:103(para)
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a "
"mouse, you'll probably want to zero this parameter."
msgstr ""
"Inkscape カリグラフィペンは、ペンの動く <firstterm>速度</firstterm> を検知で"
"きます (後述の「幅変化」を参照してください)。マウスを使用している場合はこのパ"
"ラメーターをゼロにした方がいいかもしれません。"

#: tutorial-calligraphy.xml:111(title)
msgid "Calligraphy Tool Options"
msgstr "カリグラフィツールオプション"

#: tutorial-calligraphy.xml:113(para)
#, fuzzy
msgid ""
"Switch to the Calligraphy tool by pressing <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo>, pressing the "
"<keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: <guilabel>Width</guilabel> "
"&amp; <guilabel>Thinning</guilabel>; <guilabel>Angle</guilabel> &amp; "
"<guilabel>Fixation</guilabel>; <guilabel>Caps</guilabel>; <guilabel>Tremor</"
"guilabel>, <guilabel>Wiggle</guilabel> &amp; <guilabel>Mass</guilabel>. "
"There are also two buttons to toggle tablet Pressure and Tilt sensitivity on "
"and off (for drawing tablets)."
msgstr ""
"<keycap>Ctrl+F6</keycap> キーを押す、<keycap>C</keycap> キーを押す、もしくは"
"ツールバーボタンを押してカリグラフィツールに切り替えてください。上のツールコ"
"ントロールバーには 7 つのオプション (幅と幅変化、角度と固定度、震え、およびう"
"ねりと質量) があります。またタブレットでの描画用に、タブレットの筆圧および傾"
"き検知をオン/オフする 2 つのボタンがあります。"

#: tutorial-calligraphy.xml:123(title)
msgid "Width &amp; Thinning"
msgstr "幅と幅変化"

#: tutorial-calligraphy.xml:125(para)
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom. This "
"makes sense, because the natural “unit of measure” in calligraphy is the "
"range of your hand's movement, and it is therefore convenient to have the "
"width of your pen nib in constant ratio to the size of your “drawing board” "
"and not in some real units which would make it depend on zoom. This behavior "
"is optional though, so it can be changed for those who would prefer absolute "
"units regardless of zoom. To switch to this mode, use the checkbox on the "
"tool's Preferences page (you can open it by double-clicking the tool button)."
msgstr ""
"このオプションのペアは、ペンの <firstterm>幅</firstterm> を制御します。幅の値"
"は 1 から 100 が指定できます。単位はデフォルトでは編集ウインドウサイズとの相"
"対値となりますが、ズームレベルには影響されません。これには理由があり、カリグ"
"ラフィにおける自然な「測定単位」は書き手の手の動きの範囲であり、ペン先の幅は"
"書き手の「画板」に対する一定比である方が、ズームレベルに依存した現実の単位よ"
"りも便利だからです。"

#: tutorial-calligraphy.xml:138(para)
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr ""
"ペン幅はちょくちょく変更されるものなので、ツールバーを使わずに調整することが"
"できます。<keycap>左</keycap> および <keycap>右</keycap> 矢印キーか、タブレッ"
"トの筆圧検知機能を使います。これらキーのもっとも優れている点は描画中にも動作"
"するということです。つまり、書いている最中に徐々にペンの幅を変更することがで"
"きます。"

#: tutorial-calligraphy.xml:153(para)
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr ""
"<firstterm>幅変化</firstterm> パラメーターによって、ペンの幅は速度にも依存し"
"ます。このパラメーターは -100 から 100 までの値をとり、ゼロにすると幅は速度の"
"影響を受けずに一定になり、正数なら速度に比例して細くなり、負数なら速度に比例"
"して太くなります。デフォルトの 10 は速い筆の動きで穏やかに細くなります。以下"
"にいくつかの例をあげます。すべて幅=20、角度=90で描画されています:"

#: tutorial-calligraphy.xml:169(para)
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr ""
"面白半分に、幅と幅変化の両方を 100 (最大) に設定し、きまぐれにペンを動かし描"
"いてみると、妙に自然な、神経細胞のようなシェイプができあがりました:"

#: tutorial-calligraphy.xml:184(title)
msgid "Angle &amp; Fixation"
msgstr "角度と固定度"

#: tutorial-calligraphy.xml:186(para)
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the "
"angle parameter is greyed out and the angle is determined by the tilt of the "
"pen."
msgstr ""
"幅の隣の <firstterm>角度</firstterm> はカリグラフィの中でもっとも重要なパラ"
"メーターです。ペンの角度は 0°(水平) から 90°(垂直) または -90°(反時計回りに垂"
"直) になります。タブレットの傾き検知を有効にしている場合は角度パラメーターは"
"グレーアウトされ、角度はペンの傾きにより決定されることに注意してください。"

#: tutorial-calligraphy.xml:201(para)
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands "
"and more experienced calligraphers will often vary the angle while drawing, "
"and Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr ""
"古典的なカリグラフィ書体はそれぞれ自身のペン角度を持っていました。例えば、ア"
"ンシャル体では角度 25°を採用しています。より複雑な書体やより熟練した書家はし"
"ばしば描画中に角度を変えますが、Inkscape はこれを <keycap>上</keycap> および "
"<keycap>下</keycap> 矢印キーまたはタブレットの傾き検知機能で実現しています。"
"ただし、カリグラフィの練習を始めたばかりのころは角度を一定に保つ方がよいで"
"しょう。以下に様々な角度で描かれた例を示します (固定度 = 100):"

#: tutorial-calligraphy.xml:218(para)
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr ""
"ご覧のように、ストロークはその角度に平行に描くと細くなり、垂直に描くと太くな"
"ります。右手書きのカリグラフィでは、正の角度がもっとも自然で一般的です。"

#: tutorial-calligraphy.xml:224(para)
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr ""
"最も細い部分と最も太い部分の比率のレベルは、<firstterm>固定度</firstterm> パ"
"ラメータで制御できます。この値の 100 は、常に角度フィールドで設定された角度に"
"固定されることを意味します。固定度を減らすとペンはストロークの方向に対して少"
"しずつ角度が変化します。固定度=0 で、ペンは束縛を受けずに常にストロークに対し"
"て垂直となり、角度はその効果が無視されます:"

#: tutorial-calligraphy.xml:239(para)
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke "
"width contrast (above left) are the features of antique serif typefaces, "
"such as Times or Bodoni (because these typefaces were historically an "
"imitation of fixed-pen calligraphy). Zero fixation and zero width contrast "
"(above right), on the other hand, suggest modern sans serif typefaces such "
"as Helvetica."
msgstr ""
"タイポグラフィ上の話では、最大固定度すなわち幅の対比が最大のストローク (上図"
"左) が Times や Bodoni のようなアンティークなセリフ書体の特徴になっています "
"(なぜならこれら書体は歴史的に固定ペンカリグラフィを模倣しているからです)。"

#: tutorial-calligraphy.xml:249(title)
msgid "Tremor"
msgstr "震え"

#: tutorial-calligraphy.xml:251(para)
#, fuzzy
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr ""
"<firstterm>震え</firstterm> はカリグラフィストロークにより自然な見た目を与え"
"ることを目的としています。震えはツールコントロールバー上で調整でき、値は 0 か"
"ら 100 の範囲をとります。これはストロークにわずかなむらから大胆な染みや斑点ま"
"でのなんらかの作用を及ぼします。これはこのツールでの創作の幅を大きく広げま"
"す。"

#: tutorial-calligraphy.xml:266(title)
msgid "Wiggle &amp; Mass"
msgstr "うねりと質量"

#: tutorial-calligraphy.xml:268(para)
msgid ""
"Unlike width and angle, these two last parameters define how the tool "
"“feels” rather than affect its visual output. So there won't be any "
"illustrations in this section; instead just try them yourself to get a "
"better idea."
msgstr ""
"幅や角度と違い、この 2 つのパラメーターは視覚効果に作用すると言うより、ツール"
"の「感触」を定義します。そのため、この節ではイラストは一切ありません。あなた"
"自ら試してみてください。"

#: tutorial-calligraphy.xml:274(para)
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr ""
"<firstterm>うねり</firstterm> はペンを走らせる時の紙の抵抗です。値が小さいほ"
"ど紙は「滑りやすく」なります。質量を大きくした場合、ペンは急な方向転換をしな"
"くなります。質量がゼロの場合、低いうねりはペンを大胆にうねらせます。"

#: tutorial-calligraphy.xml:281(para)
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your "
"mouse pointer and the more it smoothes out sharp turns and quick jerks in "
"your stroke. By default this value is quite small (2) so that the tool is "
"fast and responsive, but you can increase mass to get slower and smoother "
"pen."
msgstr ""
"物理学では <firstterm>質量</firstterm> は慣性の要因です。Inkscape のカリグラ"
"フィツールにおいては、質量を大きくするとマウスポインターに追随するペンの遅れ"
"がより大きくなり、きびきび動かず、ストロークがよりスムーズな形状になります。"
"デフォルトではこの値は小さく (2) になっていますので素早く反応しますが、質量を"
"増やせば、ゆっくりと、スムーズなペンになります。"

#: tutorial-calligraphy.xml:292(title)
msgid "Calligraphy examples"
msgstr "カリグラフィの例"

#: tutorial-calligraphy.xml:294(para)
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr ""
"ここまででカリグラフィツールの基本が分かったと思いますので、実際にいくつかカ"
"リグラフィを描いてみましょう。"

#: tutorial-calligraphy.xml:300(para)
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr ""
"まず最初に、ガイドとなるルーラーのペアを作成しましょう。もし斜体や草書体を書"
"こうとしているのであれば、2 つのルーラーと交差するいくつかの斜線のガイドも加"
"えます。以下に例を示します:"

#: tutorial-calligraphy.xml:313(para)
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr ""
"そして、ルーラー間が自然に手を動かすのに最も適した高さになるようにズームして"
"やってみましょう。"

#: tutorial-calligraphy.xml:318(para)
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round "
"strokes, slanted stems. Here are some letter elements for the Uncial hand:"
msgstr ""
"おそらく、ビギナーカリグラファーのあなたは最初に文字の基本要素を練習するのが"
"いいでしょう。すなわち、垂直および水平のステム、丸いストローク、斜めのステム"
"です。以下にアンシャル体におけるいくつかの文字の要素を示します:"

#: tutorial-calligraphy.xml:331(para)
msgid "Several useful tips:"
msgstr "役に立つコツ:"

#: tutorial-calligraphy.xml:336(para)
#, fuzzy
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll "
"the canvas (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>arrow</keycap></keycombo> keys) with your left hand after "
"finishing each letter."
msgstr ""
"手をタブレットの上でくつろいだ状態にしたら、そこから動かさないでください。代"
"わりに、文字を書くごとに左手でキャンバスを (<keycap>Ctrl+矢印</keycap> キー"
"で) スクロールさせましょう。"

#: tutorial-calligraphy.xml:341(para)
#, fuzzy
msgid ""
"If your last stroke is bad, just undo it (<keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>). However, if its "
"shape is good but the position or size are slightly off, it's better to "
"switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using <mousebutton>mouse</mousebutton> or keys), then "
"press <keycap>Space</keycap> again to return to Calligraphy tool."
msgstr ""
"最後のストロークに失敗したら、元に戻しましょう (<keycap>Ctrl+Z</keycap>)。た"
"だし、シェイプはうまく書けたけれども位置や大きさが少しずれていたなどの場合"
"は、一時的に選択ツールに切り替え(<keycap>スペース</keycap> キー)、マウスや"
"キーボードから移動/拡大縮小/回転などして修正しましょう。その後再び <keycap>ス"
"ペース</keycap> キーを押せばカリグラフィツールに戻ります。"

#: tutorial-calligraphy.xml:349(para)
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr ""
"語句を書いたら、選択ツールに切り替え、ステムの均一性や字間を調整しましょう。"
"でもやりすぎないでください。よいカリグラフィには多少の不規則な手書き感を残さ"
"なければなりません。文字や文字要素をコピーしたいという誘惑に負けないでくださ"
"い。筆跡はそれぞれがオリジナルでなければなりません。"

#: tutorial-calligraphy.xml:357(para)
msgid "And here are some complete lettering examples:"
msgstr "さらに、ここでいくつかの完全なレタリングの例をあげておきます:"

#: tutorial-calligraphy.xml:373(title)
msgid "Conclusion"
msgstr "最後に"

#: tutorial-calligraphy.xml:375(para)
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with "
"and may be useful in real design. Enjoy!"
msgstr ""
"カリグラフィはただ楽しいだけではありません。それは、あなたが見たり行ったりす"
"るすべての物の見方を変えてしまうかもしれないくらい、深く精神的な芸術なので"
"す。Inkscape のカリグラフィツールは、そのさわりの部分程度に用いることができる"
"だけですが、それを遊ぶには十分で、本当のデザインに役立つかもしれません。楽し"
"んでください!"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-calligraphy.xml:0(None)
msgid "translator-credits"
msgstr "Masato Hashimoto <cabezon.hashimoto@gmail.com>, 2009, 2010, 2011, 2012"

#~ msgid "angle = -90 deg"
#~ msgstr "角度 = -90°"

#~ msgid ""
#~ "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
#~ msgstr ""
#~ "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
