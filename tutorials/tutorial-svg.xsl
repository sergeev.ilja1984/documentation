<?xml version="1.0" encoding="utf-8"?>

<!DOCTYPE xsl:stylesheet [ 
<!ENTITY nbsp "&#160;"> 
<!ENTITY mdash "&#8212;">
]>

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.1"
  xmlns:func="http://exslt.org/functions"
  xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
  xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
  xmlns:exsl="http://exslt.org/common"
  xmlns="http://www.w3.org/2000/svg"
  xmlns:ink-r="org.inkscape.xslt.files"
  xmlns:ink="http://www.inkscape.org/namespaces/xslt"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  exclude-result-prefixes="func ink-r xsl"
  extension-element-prefixes="func ink-r ink exsl"
>

<xsl:output method="xml" encoding="utf-8" indent="no"/>

<!-- parameters and variables -->
<xsl:param name="ill-path"/>
<xsl:param name="ink-path"/>
<xsl:param name="language">en</xsl:param>

<xsl:variable name="font-sans">Liberation Sans,Arial,sans-serif</xsl:variable>
<xsl:variable name="font-serif">Liberation Serif,Times New Roman,serif</xsl:variable>
<xsl:variable name="font-monospace">Liberation Mono,Lucida Console,Consolas,monospace</xsl:variable>
<xsl:variable name="normal-style">
  <xsl:value-of select="concat('font-size:8;line-height:133%;font-family:', $font-serif)"/>
</xsl:variable>

<xsl:variable name="column-width">300</xsl:variable>
<xsl:variable name="margin-left">20</xsl:variable>
<xsl:variable name="margin-bottom">10</xsl:variable>

<xsl:variable name="head-margin-top">5.4</xsl:variable>
<xsl:variable name="head-margin-bottom">5</xsl:variable>
<xsl:variable name="head-fs">8</xsl:variable>
<xsl:variable name="head2-margin-top">5</xsl:variable>
<xsl:variable name="head2-margin-bottom">4.6</xsl:variable>
<xsl:variable name="head2-fs">7.5</xsl:variable>

<xsl:variable name="item-column-width">280</xsl:variable>
<xsl:variable name="item-margin-left">40</xsl:variable>

<xsl:variable name="abstract-column-width">264</xsl:variable>
<xsl:variable name="abstract-margin-left">40</xsl:variable>
<xsl:variable name="abstract-margin-bottom">12</xsl:variable>
<xsl:variable name="abstract-fs">7.2</xsl:variable>

<xsl:variable name="image-margin-left">20</xsl:variable>
<xsl:variable name="image-margin-top">8</xsl:variable>
<xsl:variable name="image-margin-bottom">12</xsl:variable>

<xsl:variable name="credits-column-width">230</xsl:variable>
<xsl:variable name="credits-margin-left">70</xsl:variable>
<xsl:variable name="credits-margin-bottom">12</xsl:variable>
<xsl:variable name="credits-bullet-margin-left">47</xsl:variable>

<xsl:variable name="header-filename">tutorial-header.svg</xsl:variable>
<xsl:variable name="footer-filename">tutorial-footer.svg</xsl:variable>
<xsl:variable name="footer-text-height">170</xsl:variable>

<xsl:variable name="id" select="generate-id()"/>
<xsl:variable name="y" select="ink-r:set-variable('y', 128)"/>

<xsl:variable name="credits">
  <credits-authors>
    <xsl:for-each select="//author">
      <xsl:value-of select="othername"/>
      <xsl:if test="position() != last()">; </xsl:if>
    </xsl:for-each>
  </credits-authors>
  <xsl:if test="$language!='en'">
    <credits-translators>
      <xsl:for-each select="//copyright"><!-- It's not present in source XML but only in translated XML geneareted by xml2po. See https://gitlab.gnome.org/GNOME/gnome-doc-utils/blob/master/xml2po/xml2po/modes/docbook.py#L151 -->
        <xsl:value-of select="substring-before( holder,' (')"/> &mdash; <xsl:value-of select="./year"/>
        <xsl:if test="position() != last()">; </xsl:if>
      </xsl:for-each>
    </credits-translators>
  </xsl:if>
  <credits-designer>Esteban Capella &mdash; 2019</credits-designer>
</xsl:variable>

<!-- custom functions -->
<func:function name="ink:normalize-without-trimming">
  <xsl:param name="string"/>
  <func:result>
    <xsl:value-of select="substring(normalize-space(concat('.',$string,'.')), 2, string-length(normalize-space(concat('.',$string,'.'))) - 2)"/> 
  </func:result>
</func:function>

<func:function name="ink:strip-leading-whitespace">
  <xsl:param name="string"/>
  <func:result>
    <xsl:choose>
      <xsl:when test="starts-with($string, ' ') or starts-with($string, '&#xD;') or starts-with($string, '&#xA;')">
        <xsl:value-of select="ink:strip-leading-whitespace(substring($string, 2, string-length(.)))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>  
  </func:result>
</func:function>

<func:function name="ink:prefixize">
  <xsl:param name="prefix"/>
  <xsl:param name="string"/>
  <func:result>
    <xsl:choose>
      <xsl:when test="contains($string, 'url(#')">
        <xsl:value-of select="concat(substring-before($string, 'url(#'), 'url(#', $prefix, ink:prefixize($prefix, substring-after($string, 'url(#')))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </func:result>
</func:function>

<!-- page skeleton -->
<xsl:template match="/">
  <xsl:comment> DO NOT EDIT THIS FILE. It is produced automatically from a DocBook source (*.xml) by tutorial-svg.xsl. </xsl:comment>
  <svg
      xmlns:xml="http://www.w3.org/XML/1998/namespace"
      xmlns:dc="http://purl.org/dc/elements/1.1/"
      xmlns:cc="http://web.resource.org/cc/"
      xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
      xmlns:svg="http://www.w3.org/2000/svg"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
      xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
      id="svg1"
      version="1.1"
      sodipodi:version="0.32"
      inkscape:version="0.92"
      width="340px"
      height="340px">
    <sodipodi:namedview
        inkscape:zoom="2"
        inkscape:cx="160"
        inkscape:cy="170"
        inkscape:window-width="780"
        inkscape:window-height="580"
        inkscape:window-x="0"
        inkscape:window-y="0"
        showborder="false"
        showguides="false"
        snaptoguides="true"/>

    <!-- header -->
    <xsl:for-each select="document($header-filename)/*/*[name() != 'sodipodi:namedview']"> 
      <xsl:copy-of select="."/>
    </xsl:for-each>

    <!-- footer  -->
    <use id="footer" xlink:href="#footer-template" sodipodi:insensitive="true" inkscape:label="Footer" />

    <g
      inkscape:label="Content"
      inkscape:groupmode="layer"
      id="layer2">

      <!-- title -->
      <text
          xml:space="preserve"
          style="font-size:16;font-family:{$font-serif};line-height:100%;fill:#000000"
          x="{$margin-left}"
          y="122"><tspan
            sodipodi:role="line"
            style="fill:#000000"><xsl:value-of select="string(book/article/articleinfo/subtitle)"/><tspan
              style="fill:#de5f3a;fill-opacity:1"> | </tspan><tspan
              style="font-style:italic"><xsl:value-of select="string(book/article/articleinfo/title)"/></tspan></tspan></text>
      <xsl:variable name="y" select="ink-r:set-variable('y', $y + $margin-bottom)"/>

      <!-- body content -->
      <xsl:apply-templates/>

      <!-- credits -->
      <xsl:variable name="y3" select="ink-r:set-variable('y', ink-r:get-variable('y') + 3*$margin-bottom)"/>
      <xsl:for-each select="exsl:node-set($credits)/*">
        <use xlink:href="{concat('#icon-', name())}" x="{$credits-bullet-margin-left}" y="{ink-r:get-variable('y') + 6}" inkscape:label="{concat('icon-', name())}" />
        <xsl:call-template name="block">
           <xsl:with-param name="gid" select="string(name())" />
           <xsl:with-param name="width" select="number($credits-column-width)" />
           <xsl:with-param name="margin-left" select="number($credits-margin-left)" />
           <xsl:with-param name="margin-bottom" select="number($credits-margin-bottom)" />
           <xsl:with-param name="style" select="concat('font-size:6;line-height:120%;font-family:', $font-sans, ';fill:#999999;text-align:left;text-anchor:start')" />
           <xsl:with-param name="text" select="string(.)" />
        </xsl:call-template>
      </xsl:for-each>

    </g>

    <!-- footer template -->
    <defs>
      <xsl:variable name="y" select="ink-r:get-variable('y')-$footer-text-height"/>
      <g transform="translate(0, {$y})"
       id="footer-template">
        <xsl:for-each select="document($footer-filename)/*/*[name() != 'sodipodi:namedview']">
          <xsl:copy-of select="."/>
        </xsl:for-each>
      </g>
    </defs>

    <!-- cleanup -->
    <xsl:variable name="dummy" select="ink-r:run('rm', concat('para-query-', generate-id(), '.svg'))"/>
  </svg>
</xsl:template>

<!-- helper templates for prefixing: mode="reid" -->
<xsl:template match="@*" mode="reid">
  <xsl:param name="prefix"/>
  <xsl:choose>
    <xsl:when test="name()='id'">
      <xsl:attribute name="{name()}">
        <xsl:value-of select="concat($prefix, .)"/>
      </xsl:attribute>
    </xsl:when>
    <xsl:when test="name()='xlink:href'">
      <xsl:attribute name="{name()}">
        <xsl:choose>
          <xsl:when test="starts-with(., '#')">
            <xsl:value-of select="concat('#', $prefix, substring-after(., '#'))"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
    </xsl:when>
    <xsl:when test="name()='inkscape:href'">
      <xsl:attribute name="{name()}">
        <xsl:choose>
          <xsl:when test="starts-with(., '#')">
            <xsl:value-of select="concat('#', $prefix, substring-after(., '#'))"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
    </xsl:when>
    <xsl:otherwise>
      <xsl:attribute name="{name()}">
        <xsl:value-of select="ink:prefixize($prefix, .)"/>
      </xsl:attribute>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="*" mode="reid">
  <xsl:param name="prefix"/>
  <xsl:element name="{name()}">
    <xsl:apply-templates mode="reid" select="@*">
      <xsl:with-param name="prefix" select="$prefix"/>
    </xsl:apply-templates>
    <xsl:apply-templates mode="reid" select="*|text()">
      <xsl:with-param name="prefix" select="$prefix"/>
    </xsl:apply-templates>
  </xsl:element>
</xsl:template>

<xsl:template match="text()" mode="reid">
  <xsl:param name="prefix"/>
  <xsl:copy/>
</xsl:template>

<!-- templates for text nodes, mostly whitespaces -->
<xsl:template match="para/text()">
  <xsl:value-of select="ink:normalize-without-trimming(.)"/>
</xsl:template>

<xsl:template match="para/text()[1][starts-with(.,'&#10;')]">
  <xsl:value-of select="ink:strip-leading-whitespace(.)"/>
</xsl:template>

<!-- templates for individual elements -->
<xsl:template match="article/title"/>
<xsl:template match="subtitle"/>
<xsl:template match="articleinfo" />

<!-- ...block level elements -->
<xsl:template match="abstract">
  <xsl:variable name="y" select="ink-r:set-variable('y', $y + $abstract-margin-bottom div 2)"/>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template name="block">
  <xsl:param name="gid" select="generate-id()" />
  <xsl:param name="width" />
  <xsl:param name="margin-left" />
  <xsl:param name="margin-top" select="number(0)" />
  <xsl:param name="margin-bottom" select="number(0)" />
  <xsl:param name="style" />
  <xsl:param name="text" />
  <xsl:variable name="y" select="ink-r:get-variable('y')"/>
  <rect id="{$gid}" width="{$width}" height="1000px" x="{$margin-left}" y="{$y + $margin-top}" style="display:none"/>
  <flowRoot style="{$style}">
    <flowRegion>
      <use
          xlink:href="#{$gid}"
          y="0"
          x="0" />
    </flowRegion>
    <xsl:choose>
      <xsl:when test="$text != ''"><flowDiv xml:space="preserve"><xsl:value-of select="string($text)" /></flowDiv></xsl:when>
      <xsl:otherwise><flowDiv xml:space="preserve"><xsl:apply-templates/></flowDiv></xsl:otherwise>
    </xsl:choose>
  </flowRoot>
  <xsl:variable name="current-h">
    <xsl:document href="para-query-{$id}.svg" encoding="utf-8" method="xml" omit-xml-declaration="yes" indent="yes">
      <svg width="1600px" height="400px">
        <rect id="rectID" width="{$width}" height="1000px" x="{$margin-left}" y="{$margin-top}" style="fill:none"/>
        <flowRoot
            id="flowRootID" style="{$style}">
          <flowRegion>
            <use
                xlink:href="#rectID"
                y="0"
                x="0" />
          </flowRegion>
          <xsl:choose>
            <xsl:when test="$text != ''"><flowDiv xml:space="preserve"><xsl:value-of select="string($text)" /></flowDiv></xsl:when>
            <xsl:otherwise><flowDiv xml:space="preserve"><xsl:apply-templates/></flowDiv></xsl:otherwise>
          </xsl:choose>
        </flowRoot>
      </svg>
    </xsl:document>
    <xsl:value-of select="ink-r:run-shell($ink-path, concat('para-query-', $id, '.svg --query-height --query-id flowRootID'))"/>
  </xsl:variable>
  <xsl:variable name="y_new" select="ink-r:set-variable('y', $y + $margin-top + $current-h + $margin-bottom)"/>
</xsl:template>

<xsl:template match="sect1/title">
  <xsl:call-template name="block">
     <xsl:with-param name="width" select="number($column-width)" />
     <xsl:with-param name="margin-left" select="number($margin-left)" />
     <xsl:with-param name="margin-top" select="number($head-margin-top)" />
     <xsl:with-param name="margin-bottom" select="number($head-margin-bottom)" />
     <xsl:with-param name="style" select="concat('font-size:', $head-fs, ';font-style:normal;font-weight:bold;fill:#000000;fill-opacity:1;stroke:none;font-family:', $font-sans)" />
  </xsl:call-template>
</xsl:template>

<xsl:template match="sect2/title">
  <xsl:call-template name="block">
     <xsl:with-param name="width" select="number($column-width)" />
     <xsl:with-param name="margin-left" select="number($margin-left)" />
     <xsl:with-param name="margin-top" select="number($head2-margin-top)" />
     <xsl:with-param name="margin-bottom" select="number($head2-margin-bottom)" />
     <xsl:with-param name="style" select="concat('font-size:', $head2-fs, ';font-style:italic;font-weight:bold;font-family:', $font-sans)" />
  </xsl:call-template>
</xsl:template>

<xsl:template match="abstract/para">
  <xsl:call-template name="block">
     <xsl:with-param name="width" select="number($abstract-column-width)" />
     <xsl:with-param name="margin-left" select="number($abstract-margin-left)" />
     <xsl:with-param name="margin-bottom" select="number($abstract-margin-bottom)" />
     <xsl:with-param name="style" select="concat('font-size:', $abstract-fs, ';font-style:italic;line-height:150%;font-family:', $font-sans)" />
  </xsl:call-template>
</xsl:template>

<xsl:template match="sect1/para|sect2/para|article/para">
  <xsl:call-template name="block">
     <xsl:with-param name="width" select="number($column-width)" />
     <xsl:with-param name="margin-left" select="number($margin-left)" />
     <xsl:with-param name="margin-bottom" select="number($margin-bottom)" />
     <xsl:with-param name="style" select="string($normal-style)" />
  </xsl:call-template>
</xsl:template>

<xsl:template match="listitem/para">
  <xsl:variable name="nesting" select="number(count(ancestor::itemizedlist) + 1)" />
  <xsl:variable name="nested-margin" select="number(number($item-margin-left - $margin-left) * $nesting)" />
  <xsl:variable name="y" select="ink-r:get-variable('y')"/>
  <circle cx="0" cy="0" r="2" transform="translate({$nested-margin - 5}, {$y + 6})"/>
  <xsl:call-template name="block">
     <xsl:with-param name="width" select="number($item-column-width + $item-margin-left - $nested-margin)" />
     <xsl:with-param name="margin-left" select="number($nested-margin)" />
     <xsl:with-param name="margin-bottom" select="number($margin-bottom)" />
     <xsl:with-param name="style" select="string($normal-style)" />
  </xsl:call-template>
</xsl:template>

<!-- ...inline elements -->
<xsl:template match="keycombo">
  <xsl:for-each select="keycap|mousebutton">
    <xsl:apply-templates select="."/>
    <xsl:choose>
      <xsl:when test="position() != last()">+</xsl:when>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<xsl:template match="keycap">
  <xsl:choose>
    <!-- target values: Ctrl, Left Ctrl, Right Ctrl, Alt, Left Alt, Right Alt, Shift -->
    <xsl:when test="@function='control' or @function='shift' or @function='alt'">
      <flowSpan style="font-weight:bold"><xsl:value-of select="text()"/></flowSpan>
    </xsl:when>
    <xsl:otherwise>
      <flowSpan style="font-weight:bold"><xsl:value-of select="text()"/></flowSpan>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="mousebutton">
  <!-- TODO {@role} -->
  <flowSpan style="font-weight:bold"><xsl:value-of select="text()"/></flowSpan>
</xsl:template>

<xsl:template match="guimenu">
  <flowSpan style="font-family:{$font-sans}" fill="#6495ED"><xsl:apply-templates/></flowSpan>
</xsl:template>

<xsl:template match="guisubmenu">
  <flowSpan style="font-family:{$font-sans}" fill="#6495ED"><xsl:apply-templates/></flowSpan>
</xsl:template>

<xsl:template match="guimenuitem">
  <flowSpan style="font-family:{$font-sans}" fill="#6495ED"><xsl:apply-templates/></flowSpan>
</xsl:template>

<xsl:template match="menuchoice">
  <xsl:for-each select="guimenu|guisubmenu|guimenuitem">
    <xsl:apply-templates select="."/>
    <xsl:choose>
      <xsl:when test="position() != last()">⇒</xsl:when>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<xsl:template match="guibutton">
  <flowSpan style="font-family:{$font-sans}"><xsl:apply-templates/></flowSpan>
</xsl:template>

<xsl:template match="guilabel">
  <flowSpan style="font-family:{$font-sans}"><xsl:apply-templates/></flowSpan>
</xsl:template>

<xsl:template match="emphasis">
  <flowSpan style="font-style:italic"><xsl:apply-templates/></flowSpan>
</xsl:template>

<xsl:template match="firstterm">
  <flowSpan style="font-style:italic"><xsl:apply-templates/></flowSpan>
</xsl:template>

<xsl:template match="command">
  <flowSpan style="font-family:{$font-monospace}"><xsl:apply-templates/></flowSpan>
</xsl:template>

<xsl:template match="code">
  <flowSpan style="font-family:{$font-monospace}"><xsl:apply-templates/></flowSpan>
</xsl:template>

<xsl:template match="ulink">
  <flowSpan style="font-weight:bold; font-family:{$font-sans}"><xsl:apply-templates/></flowSpan>
</xsl:template>

<xsl:template match="informalfigure">
  <xsl:variable name="y2" select="ink-r:get-variable('y')"/>
  <xsl:variable name="y" select="ink-r:set-variable('y', $y2 + $image-margin-top)"/>

  <xsl:variable name="filename" select="mediaobject/imageobject/imagedata/@fileref"/>
  <xsl:variable name="filename_stripped" select="substring-before($filename, '.')"/>
  <xsl:variable name="filename_localized">
    <xsl:choose>
      <xsl:when test="$language = 'en'"><xsl:value-of select="$filename"/></xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="ink-r:exists(concat($ill-path, $filename_stripped, '-', $language, '.svg'))"><xsl:value-of select="concat($filename_stripped, '-', $language, '.svg')"/></xsl:when>
          <xsl:otherwise><xsl:value-of select="$filename"/></xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:if test="document(concat($ill-path, $filename_localized))/*/*[name()='defs']/*">
    <xsl:apply-templates mode="reid" select="document(concat($ill-path, $filename_localized))/*/*[name()='defs']">
      <xsl:with-param name="prefix" select="$filename_localized"/>
    </xsl:apply-templates>
  </xsl:if>

  <xsl:for-each select="document(concat($ill-path, $filename_localized))/*/*[@inkscape:groupmode='layer']/*"> 
    <xsl:variable name="old_t" select="@transform"/>
    <xsl:copy>
      <xsl:choose>
        <xsl:when test="not(name()='use') and not(@sodipodi:type='inkscape:offset')">
          <xsl:attribute name="transform"><xsl:value-of select="concat('translate(', $image-margin-left, ', ', $y, ')',  ' ',  $old_t)"/></xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="transform"><xsl:value-of select="concat('translate(', $image-margin-left, ', ', $y, ')',  ' ',  $old_t, ' translate(', -$image-margin-left, ', ', -$y, ')')"/></xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates mode="reid" select="@*[name()!='transform']">
        <xsl:with-param name="prefix" select="$filename_localized"/>
      </xsl:apply-templates>
      <xsl:apply-templates mode="reid" select="*|text()">
        <xsl:with-param name="prefix" select="$filename_localized"/>
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:for-each>

  <xsl:variable name="current-h"><xsl:value-of select="ink-r:run-shell($ink-path, concat($ill-path, $filename_localized, ' --query-height'))"/></xsl:variable>
  <xsl:variable name="y_new" select="ink-r:set-variable('y', $y + $current-h + $image-margin-bottom)"/>
</xsl:template>

</xsl:stylesheet>
