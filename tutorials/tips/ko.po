msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2020-04-28 20:51+0200\n"
"PO-Revision-Date: 2019-05-29 19:49+0900\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.2\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: tips-f06.svg:136(format) tips-f05.svg:558(format) tips-f04.svg:70(format)
#: tips-f03.svg:49(format) tips-f02.svg:49(format) tips-f01.svg:49(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: tutorial-tips.xml:7(title)
msgid "Tips and Tricks"
msgstr "팁과 기술"

#: tutorial-tips.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-tips.xml:13(para)
msgid ""
"This tutorial will demonstrate various tips and tricks that users have "
"learned through the use of Inkscape and some “hidden” features that can help "
"you speed up production tasks."
msgstr ""
"본 튜토리얼은 사용자가 잉크스케이프를 사용하여 학습한 다양한 팁과 기술과 프로"
"덕션 작업의 속도를 높일 수 있는 일부 \"숨겨진\" 기능을 시연할 것입니다."

#: tutorial-tips.xml:21(title)
msgid "Radial placement with Tiled Clones"
msgstr "타일형 클론을 사용한 반지름 배치"

#: tutorial-tips.xml:22(para)
#, fuzzy
msgid ""
"It's easy to see how to use the <guimenuitem>Create Tiled Clones</"
"guimenuitem> dialog for rectangular grids and patterns. But what if you need "
"<firstterm>radial</firstterm> placement, where objects share a common center "
"of rotation? It's possible too!"
msgstr ""
"직사각형 그리드 및 패턴에 대해 <command> 타일형 클론 생성</command> 대화상자"
"를 사용하는 방법을 쉽게 알 수 있습니다. 그러나 물체가 공통의 회전 중심을 공유"
"하는 <command>방사혀</command> 배치가 필요하다면? 가능합니다!"

#: tutorial-tips.xml:27(para)
msgid ""
"If your radial pattern only needs to have 3, 4, 6, 8, or 12 elements, then "
"you can try the P3, P31M, P3M1, P4, P4M, P6, or P6M symmetries. These will "
"work nicely for snowflakes and the like. A more general method, however, is "
"as follows."
msgstr ""
"만약 당신의 방사형 패턴이 3, 4, 6, 8 또는 12개의 요소만 필요로 한다면, 당신"
"은 P3, P31M, P3M1, P4, P6 또는 P6M 대칭을 시도할 수 있습니다. 이것들은 눈송"
"이 같은 것에 잘 어울릴 것입니다. 그러나 보다 일반적인 방법은 다음과 같습니다."

#: tutorial-tips.xml:32(para)
#, fuzzy
msgid ""
"Choose the P1 symmetry (simple translation) and then <emphasis>compensate</"
"emphasis> for that translation by going to the <guimenuitem>Shift</"
"guimenuitem> tab and setting <guilabel>Per row/Shift Y</guilabel> and "
"<guilabel>Per column/Shift X</guilabel> both to -100%. Now all clones will "
"be stacked exactly on top of the original. All that remains to do is to go "
"to the <guimenuitem>Rotation</guimenuitem> tab and set some rotation angle "
"per column, then create the pattern with one row and multiple columns. For "
"example, here's a pattern made out of a horizontal line, with 30 columns, "
"each column rotated 6 degrees:"
msgstr ""
"P1 대칭(단순 변환)을 선택한 다음<command>Shift</command>탭으로 이동하여 "
"<command>Per row/Shift Y</command> 및 <command>Per column/Shift X</command>"
"를 -100%로 설정하여 해당 변환을 <emphasis>보정</emphasis>하십시오. 이제 모든 "
"복제품은 원본 위에 정확히 쌓일 것입니다. <command>Rotation</command> 탭으로 "
"가서 칼럼당 약간의 회전 각도를 설정한 다음, 한 행과 여러 개의 열로 패턴을 만"
"들 수 있습니다. 예를 들어, 다음은 30개의 열을 가지고 각 열은 6도씩 회전된 수"
"평선으로 만들어진 패턴입니다:"

#: tutorial-tips.xml:49(para)
msgid ""
"To get a clock dial out of this, all you need to do is cut out or simply "
"overlay the central part by a white circle (to do boolean operations on "
"clones, unlink them first)."
msgstr ""
"이것으로부터 시계 다이얼을 얻기 위해서, 당신이 해야 할 일은 중앙 부분을 흰 원"
"으로 잘라내거나 간단히 오버레이하는 것입니다(복제에 대한 부울 연산을 하기 위"
"해서, 먼저 연결을 해제하세요)."

#: tutorial-tips.xml:53(para)
msgid ""
"More interesting effects can be created by using both rows and columns. "
"Here's a pattern with 10 columns and 8 rows, with rotation of 2 degrees per "
"row and 18 degrees per column. Each group of lines here is a “column”, so "
"the groups are 18 degrees from each other; within each column, individual "
"lines are 2 degrees apart:"
msgstr ""
"행과 열을 모두 사용함으로써 더 흥미로운 효과를 만들 수 있습니다. 다음은 한 줄"
"에 2도, 한 줄에 18도 회전된 10개의 기둥과 8개의 행을 가진 패턴입니다. 여기서 "
"각 선 그룹은 \"열\"이므로 그룹들은 서로 18도이고, 각 열 내에서 개별 선은 2도 "
"차이가 납니다:"

#: tutorial-tips.xml:67(para)
#, fuzzy
msgid ""
"In the above examples, the line was rotated around its center. But what if "
"you want the center to be outside of your shape? Just click on the object "
"twice with the Selector tool to enter rotation mode. Now move the object's "
"rotation center (represented by a small cross-shaped handle) to the point "
"you would like to be the center of the rotation for the Tiled Clones "
"operation. Then use <guimenuitem>Create Tiled Clones</guimenuitem> on the "
"object. This is how you can do nice “explosions” or “starbursts” by "
"randomizing scale, rotation, and possibly opacity:"
msgstr ""
"위의 예에서 선은 중앙을 중심으로 회전하였습니다. 하지만 만약 당신은 중앙이 모"
"양 밖에 있기를 바란다면? 회전 모드로 들어가려면 Selector 도구를 사용하여 개체"
"를 두 번 클릭하십시오. 이제 타일 클론 작동을 위해 회전 중심이 되고자 하는 지"
"점(소형 십자형 핸들로 표시됨)으로 개체의 회전 중심을 이동하십시오. 그런 다음 "
"개체에 대해 <command>타일된 복제본 만들기</command>를 사용하십시오. 크기 조"
"절, 회전 및 불투명도를 랜덤화하여 멋진 \"폭발\" 또는 \"스타버스트\"를 수행할 "
"수 있는 방법은 다음과 같습니다:"

#: tutorial-tips.xml:85(title)
msgid "How to do slicing (multiple rectangular export areas)?"
msgstr "자르는 방법(여러 직사각형 내보내기 영역)은 무엇일까?"

#: tutorial-tips.xml:86(para)
#, fuzzy
msgid ""
"Create a new layer, in that layer create invisible rectangles covering parts "
"of your image. Make sure your document uses the px unit (default), turn on "
"grid and snap the rects to the grid so that each one spans a whole number of "
"px units. Assign meaningful ids to the rects, and export each one to its own "
"file (<menuchoice><guimenu>File</guimenu><guimenuitem>Export PNG Image</"
"guimenuitem></menuchoice> (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>E</keycap></"
"keycombo>)). Then the rects will remember their export filenames. After "
"that, it's very easy to re-export some of the rects: switch to the export "
"layer, use <keycap>Tab</keycap> to select the one you need (or use Find by "
"id), and click <guibutton>Export</guibutton> in the dialog. Or, you can "
"write a shell script or batch file to export all of your areas, with a "
"command like:"
msgstr ""
"새 도면층을 작성하고, 그 도면층에서 이미지의 일부를 덮는 보이지 않는 사각형"
"을 생성하세요. 문서가 px 단위(기본값)를 사용하는지 확인하고, 그리드를 켜고 "
"각 작업량이 전체 px 단위로 확장되도록 그리드에 작업창을 스냅하십시오. 작업창"
"에 의미 있는 ID를 할당하고 각 ID를 자체 파일로 내보내십시오(<command>파일 "
"&gt; PNG 이미지 내보내기 </command>(<keycap>Shift+Ctrl+E </keycap>)). 그러면 "
"작업창은 그들이 내보낸 파일 이름을 기억할 것입니다. 그러고 나서는 다음 작업 "
"일부를 다시 내보내기가 매우 쉬워집니다. 즉, 내보내기 계층으로 전환하거나, 탭"
"을 사용하여 필요한 작업(또는 ID별로 찾기)을 선택하고, 대화상자에서 내보내기"
"를 클릭하세요. 또는 다음과 같은 명령을 사용하여 셸 스크립트 또는 배치 파일을 "
"작성하여 모든 영역을 내보낼 수 있습니다:"

#: tutorial-tips.xml:98(command)
msgid "inkscape -i area-id -t filename.svg"
msgstr "inkscape -i area-id -t filename.svg"

#: tutorial-tips.xml:100(para)
#, fuzzy
msgid ""
"for each exported area. The <command>-t</command> switch tells it to use the "
"remembered filename hint, otherwise you can provide the export filename with "
"the <command>-e</command> switch. Alternatively, you can use the "
"<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Web</"
"guisubmenu><guimenuitem>Slicer</guimenuitem></menuchoice> extensions, or "
"<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Export</"
"guisubmenu><guimenuitem>Guillotine</guimenuitem></menuchoice> for similar "
"results."
msgstr ""
"각 내보낸 지역마다. -t 스위치는 기억된 파일 이름 힌트를 사용하도록 지시하고, "
"그렇지 않으면 내보내기 파일 이름을 -e 스위치와 함께 제공할 수 있습니다. 이것"
"의 대안으로 당신은 유사한 결과를 도출하기 위해 <command> 확장 &gt; 웹 &gt; "
"Slicer</command> 확장, 또는 <command> 확장 &gt; 내보내기 &gt; Guillotine </"
"command>를 사용할 수 있습니다."

#: tutorial-tips.xml:108(title)
msgid "Non-linear gradients"
msgstr "비선형 그라데이션"

#: tutorial-tips.xml:109(para)
#, fuzzy
msgid ""
"The version 1.1 of SVG does not support non-linear gradients (i.e. those "
"which have a non-linear translations between colors). You can, however, "
"emulate them by <firstterm>multistop</firstterm> gradients."
msgstr ""
"SVG의 1.1 버전은 비선형 그라데이션을 지원하지 않습니다(즉, 색상 간에 비선형 "
"변환을 갖는 그라데이션). 그러나 당신은 그것들을 <firstterm>multistop</"
"firstterm> 그라데이션으로 모방할 수 있습니다."

#: tutorial-tips.xml:114(para)
#, fuzzy
msgid ""
"Start with a simple two-stop gradient (you can assign that in the Fill and "
"Stroke dialog or use the gradient tool). Now, with the gradient tool, add a "
"new gradient stop in the middle; either by double-clicking on the gradient "
"line, or by selecting the square-shaped gradient stop and clicking on the "
"button <guimenuitem>Insert new stop</guimenuitem> in the gradient tool's "
"tool bar at the top. Drag the new stop a bit. Then add more stops before and "
"after the middle stop and drag them too, so that the gradient looks smooth. "
"The more stops you add, the smoother you can make the resulting gradient. "
"Here's the initial black-white gradient with two stops:"
msgstr ""
"간단한 투 스톱 그라데이션으로 시작하십시오(채우기 및 스트로크 대화 상자에서 "
"이를 지정하거나 그라데이션 도구를 사용하십시오). 이제 그라데이션 라인을 두 "
"번 클릭하거나 사각형 그라데이션 스톱을 선택하고 상단의 그라데이션 툴바에 있"
"는 <command> 새 스톱 추가</command> 버튼을 클릭하는 방법으로 그라데이션 도구"
"를 사용하여 중간에 새 그라데이션 스톱을 추가하십시오. 새 스톱을 약간 끌어다 "
"놓으십시오. 그런 다음 중간 정지 전후에 정지를 더하고 또한 끌어서 그라데이션"
"이 매끄럽게 보이도록 하세요. 스톱을 더 많이 추가할수록 결과의 기울기를 부드럽"
"게 만들 수 있습니다. 다음은 두 개의 스톱이 있는 초기 흑백 그라데이션입니다:"

#: tutorial-tips.xml:131(para)
msgid ""
"And here are various “non-linear” multi-stop gradients (examine them in the "
"Gradient Editor):"
msgstr ""
"그리고 다음은 다양한 \"비선형\" 멀티 스톱 그라데이션이 입니다 (그라데이션 편"
"집기에서 검토):"

#: tutorial-tips.xml:145(title)
msgid "Excentric radial gradients"
msgstr "기이한 방사형 그라데이션"

#: tutorial-tips.xml:146(para)
#, fuzzy
msgid ""
"Radial gradients don't have to be symmetric. In Gradient tool, drag the "
"central handle of an elliptic gradient with <keycap function=\"shift"
"\">Shift</keycap>. This will move the x-shaped <firstterm>focus handle</"
"firstterm> of the gradient away from its center. When you don't need it, you "
"can snap the focus back by dragging it close to the center."
msgstr ""
"방사형의 그라데이션은 대칭적일 필요가 없습니다. 그라데이션 도구에서 "
"<keycap>Shift</keycap>를 사용하여 타원 그라데이션의 중앙 핸들을 끌어다 놓으십"
"시오. 이렇게 하면 경사도의 x자형 <firstterm>포커스 핸들</firstterm>이 중심에"
"서 멀어지게 됩니다. 필요하지 않을 때는 중심으로 가까이 끌어서 초점을 다시 맞"
"추면 됩니다."

#: tutorial-tips.xml:162(title)
msgid "Aligning to the center of the page"
msgstr "페이지 중앙에 정렬하기"

#: tutorial-tips.xml:163(para)
#, fuzzy
msgid ""
"To align something to the center or side of a page, select the object or "
"group and then choose <guimenuitem>Page</guimenuitem> from the "
"<guilabel>Relative to:</guilabel> list in the <guimenuitem>Align and "
"Distribute</guimenuitem> dialog (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>A</keycap></"
"keycombo>)."
msgstr ""
"페이지의 중앙 또는 측면에 정렬하려면 개체 또는 그룹을 선택한 다음 정렬 및 "
"Distribute 대화 상자(<keycap>Shift+Ctrl+A</keycap>)의 <command>Relative to:</"
"command> 목록에서 <command>페이지</command>를 선택하십시오."

#: tutorial-tips.xml:171(title)
msgid "Cleaning up the document"
msgstr "문서 정리하기"

#: tutorial-tips.xml:172(para)
#, fuzzy
msgid ""
"Many of the no-longer-used gradients, patterns, and markers (more precisely, "
"those which you edited manually) remain in the corresponding palettes and "
"can be reused for new objects. However if you want to optimize your "
"document, use the <guimenuitem>Clean Up Document</guimenuitem> command in "
"<guimenu>File</guimenu> menu. It will remove any gradients, patterns, or "
"markers which are not used by anything in the document, making the file "
"smaller."
msgstr ""
"더 이상 사용하지 않는 그라데이션, 패턴, 마커(더 정확히 말하면 수동으로 편집"
"한 것)의 상당수는 해당 팔레트에 남아 있으며 새로운 오브젝트에 재사용할 수 있"
"습니다. 그러나 문서를 최적화하려면 파일 메뉴의 <command>문서 정리</command> "
"명령을 사용하십시오. 문서의 어떤 항목에서도 사용하지 않는 그라데이션, 패턴 또"
"는 마커를 제거하여 파일을 작게 만드세요."

#: tutorial-tips.xml:181(title)
msgid "Hidden features and the XML editor"
msgstr "숨김 기능 및 XML 편집기"

#: tutorial-tips.xml:182(para)
#, fuzzy
msgid ""
"The XML editor (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>X</keycap></keycombo>) allows you "
"to change almost all aspects of the document without using an external text "
"editor. Also, Inkscape usually supports more SVG features than are "
"accessible from the GUI. The XML editor is one way to get access to these "
"features (if you know SVG)."
msgstr ""
"XML 편집기 (<keycap>Shift+Ctrl+X</keycap>)를 사용하면 외부 텍스트 편집기를 사"
"용하지 않고도 문서의 거의 모든 측면을 변경할 수 있습니다. 또한 Inkscape는 보"
"통 GUI에서 접근할 수 있는 것보다 더 많은 SVG 기능을 지원합니다. XML 편집기는 "
"이러한 특징(SVG를 알고 있는 경우)에 대한 액세스 권한을 얻는 한 가지 방법입니"
"다."

#: tutorial-tips.xml:191(title)
msgid "Changing the rulers' unit of measure"
msgstr "통치자의 측정 단위 변경"

#: tutorial-tips.xml:192(para)
#, fuzzy
msgid ""
"In the default template, the unit of measure used by the rulers is mm. This "
"is also the unit used in displaying coordinates at the lower-left corner and "
"preselected in all units menus. (You can always hover your "
"<mousebutton>mouse</mousebutton> over a ruler to see the tooltip with the "
"units it uses.) To change this, open <guimenuitem>Document Properties</"
"guimenuitem> (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>D</keycap></keycombo>) and change "
"the <guimenuitem>Display units</guimenuitem> on the <guimenuitem>Page</"
"guimenuitem> tab."
msgstr ""
"기본 템플릿에서, 통치자가 사용하는 측정 단위는 mm입니다. 이 장치는 왼쪽 아래 "
"모서리에 좌표를 표시하는 데 사용되며 모든 단위 메뉴에서 미리 선택됩니다. (항"
"상 마우스를 눈금자 위에 올려 놓아 사용하는 단위로 툴팁을 볼 수 있습니다) 이"
"를 변경하려면 <command>문서 속성</command> (<keycap>Shift+Ctrl+D</keycap>)을 "
"열고 <command>페이지 </command>탭에서 <command>표시 단위</command>를 변경하십"
"시오."

#: tutorial-tips.xml:203(title)
msgid "Stamping"
msgstr "스탬프"

#: tutorial-tips.xml:204(para)
#, fuzzy
msgid ""
"To quickly create many copies of an object, use <firstterm>stamping</"
"firstterm>. Just drag an object (or scale or rotate it), and while holding "
"the <mousebutton role=\"click\">mouse</mousebutton> button down, press "
"<keycap>Space</keycap>. This leaves a “stamp” of the current object shape. "
"You can repeat it as many times as you wish."
msgstr ""
"오브젝트의 많은 복사본을 빠르게 만들려면 <firstterm>스탬프</firstterm>를 사용"
"하십시오. 오브젝트를 끌어서(또는 크기를 조정하거나 회전하여) 마우스 버튼을 누"
"른 상태에서 <keycap>스페이스 바</keycap>를 누르십시오. 이것은 현재 오브젝트 "
"모양의 \"스탬프\"를 남깁니다. 이것을 얼마든지 반복할 수 있다."

#: tutorial-tips.xml:213(title)
msgid "Pen tool tricks"
msgstr "펜 도구 기술"

#: tutorial-tips.xml:214(para)
msgid ""
"In the Pen (Bezier) tool, you have the following options to finish the "
"current line:"
msgstr ""
"펜(Bezier) 도구에서 현재 라인을 완료하는 데 사용할 수 있는 옵션은 다음과 같습"
"니다:"

#: tutorial-tips.xml:218(para)
msgid "Press <keycap>Enter</keycap>"
msgstr "<keycap>Enter</keycap>를 누르세요"

#: tutorial-tips.xml:221(para)
#, fuzzy
msgid ""
"<mousebutton role=\"double-click\">Double click</mousebutton> with the left "
"mouse button"
msgstr "마우스 왼쪽 버튼을 더블클릭 하세요"

#: tutorial-tips.xml:224(para)
msgid ""
"Click with the <mousebutton role=\"right-click\">right</mousebutton> mouse "
"button"
msgstr ""

#: tutorial-tips.xml:227(para)
msgid "Select another tool"
msgstr "다른 도구를 선택하세요"

#: tutorial-tips.xml:232(para)
#, fuzzy
msgid ""
"Note that while the path is unfinished (i.e. is shown green, with the "
"current segment red) it does not yet exist as an object in the document. "
"Therefore, to cancel it, use either <keycap>Esc</keycap> (cancel the whole "
"path) or <keycap>Backspace</keycap> (remove the last segment of the "
"unfinished path) instead of <guimenuitem>Undo</guimenuitem>."
msgstr ""
"경로가 완료되지 않은 상태(즉, 현재 세그먼트가 빨간색인 경우 녹색 표시)에는 아"
"직 문서에서 오브젝트로 존재하지 않는다는 점에 유의하십시오. 따라서 취소하려"
"면 <command>Undo</command>(실행 취소) 대신 <keycap>Esc</keycap>(전체 경로 취"
"소) 또는 <keycap>Backspace</keycap>(미완료 경로의 마지막 세그먼트 제거)를 사"
"용하십시오."

#: tutorial-tips.xml:239(para)
#, fuzzy
msgid ""
"To add a new subpath to an existing path, select that path and start drawing "
"with <keycap function=\"shift\">Shift</keycap> from an arbitrary point. If, "
"however, what you want is to simply <emphasis>continue</emphasis> an "
"existing path, Shift is not necessary; just start drawing from one of the "
"end anchors of the selected path."
msgstr ""
"기존 경로에 새 하위 경로를 추가하려면 해당 경로를 선택하고 임의 지점에서 "
"<keycap>Shift</keycap>를 사용하여 그리기를 시작하십시오. 그러나 기존 경로를 "
"단순히 <emphasis>계속</emphasis>하려면 Shift가 필요하지 않으므로 선택한 경로"
"의 끝 앵커 중 하나에서 그림을 그리기 시작하십시오."

#: tutorial-tips.xml:248(title)
msgid "Entering Unicode values"
msgstr "유니코드 값 입력"

#: tutorial-tips.xml:249(para)
#, fuzzy
msgid ""
"While in the Text tool, pressing <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>U</keycap></keycombo> toggles between Unicode and "
"normal mode. In Unicode mode, each group of 4 hexadecimal digits you type "
"becomes a single Unicode character, thus allowing you to enter arbitrary "
"symbols (as long as you know their Unicode codepoints and the font supports "
"them). To finish the Unicode input, press <keycap>Enter</keycap>. For "
"example, <keycombo action=\"seq\"><keycap function=\"control\">Ctrl</"
"keycap><keycap>U</keycap><keycap>2</keycap><keycap>0</keycap><keycap>1</"
"keycap><keycap>4</keycap><keycap>Enter</keycap></keycombo> inserts an em-"
"dash (—). To quit the Unicode mode without inserting anything press "
"<keycap>Esc</keycap>."
msgstr ""
"텍스트 도구에서 <keycap>Ctrl+U</keycap>를 누르면 유니코드와 일반 모드가 전환"
"됩니다. 유니코드 모드에서는 사용자가 입력하는 4개의 16진수 자릿수의 각 그룹"
"이 단일 유니코드 문자가 되므로 임의 기호를 입력할 수 있습니다(유니코드 코덱"
"을 알고 글꼴이 지원하는 한). 유니코드 입력을 완료하려면 <keycap>Enter</"
"keycap> 키를 누르십시오. 예를 들어 <keycap>Ctrl+U 2 0 1 4 Enter</keycap>은 "
"em-dash(-)를 삽입합니다. 아무것도 삽입하지 않고 유니코드 모드를 종료하려면 "
"<keycap>Esc</keycap> 키를 누르십시오."

#: tutorial-tips.xml:258(para)
#, fuzzy
msgid ""
"You can also use the <menuchoice><guimenu>Text</guimenu><guimenuitem>Unicode "
"Characters</guimenuitem></menuchoice> dialog to search for and insert glyphs "
"into your document."
msgstr ""
"또한 <command>텍스트 &gt; 글리프</command> 대화 상자를 사용하여 문서를 검색하"
"고 글리프를 삽입할 수도 있습니다."

#: tutorial-tips.xml:265(title)
msgid "Using the grid for drawing icons"
msgstr "그리드를 사용하여 아이콘 그리기"

#: tutorial-tips.xml:266(para)
#, fuzzy
msgid ""
"Suppose you want to create a 24x24 pixel icon. Create a 24x24 px canvas (use "
"the <guimenuitem>Document Preferences</guimenuitem>) and set the grid to 0.5 "
"px (48x48 gridlines). Now, if you align filled objects to <emphasis>even</"
"emphasis> gridlines, and stroked objects to <emphasis>odd</emphasis> "
"gridlines with the stroke width in px being an even number, and export it at "
"the default 96dpi (so that 1 px becomes 1 bitmap pixel), you get a crisp "
"bitmap image without unneeded antialiasing."
msgstr ""
"24x24 픽셀 아이콘을 생성하고 싶을 때에는, 24x24 px 캔버스를 만들고(<command>"
"문서 Preferences</command> 설정 사용) 그리드를 0.5 px(48x48 격자선)로 설정하"
"십시오. 이제 채워진 오브젝트를 <emphasis>짝수</emphasis> 격자선에 맞추고 px"
"의 스트로크 폭을 가진 <emphasis>홀수</emphasis> 격자선에 오브젝트를 내보내어 "
"기본 96dpi(1px가 1비트맵 픽셀이 되도록)로 내보내면 불필요한 에일리어싱 제거 "
"없이 바삭한 비트맵 이미지를 얻을 수 있습니다."

#: tutorial-tips.xml:277(title)
msgid "Object rotation"
msgstr "오브젝트 회전"

#: tutorial-tips.xml:278(para)
#, fuzzy
msgid ""
"When in the Selector tool, <mousebutton role=\"click\">click</mousebutton> "
"on an object to see the scaling arrows, then <mousebutton role=\"click"
"\">click</mousebutton> again on the object to see the rotation and skew "
"arrows. If the arrows at the corners are clicked and dragged, the object "
"will rotate around the center (shown as a cross mark). If you hold down the "
"<keycap function=\"shift\">Shift</keycap> key while doing this, the rotation "
"will occur around the opposite corner. You can also drag the rotation center "
"to any place."
msgstr ""
"선택 도구에서 오브젝트를 <keycap>클릭</keycap>하여 크기 조정 화살표를 표시한 "
"다음 오브젝트를 다시 <keycap>클릭</keycap>하여 회전 및 스큐 화살표를 확인하십"
"시오. 모서리의 화살표를 클릭하고 끌면 물체가 중심을 중심으로 회전합니다(십자 "
"표시로 표시). 이렇게 하는 동안 <keycap>Shift</keycap> 키를 누른 채로 있으면 "
"반대쪽 코너를 중심으로 회전합니다. 당신은 회전 중심을 아무 곳으로나 끌 수도 "
"있습니다."

#: tutorial-tips.xml:287(para)
#, fuzzy
msgid ""
"Or, you can rotate from keyboard by pressing <keycap>[</keycap> and "
"<keycap>]</keycap> (by 15 degrees) or <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>[</keycap></keycombo> and <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>]</keycap></keycombo> (by 90 degrees). The "
"same <keycap>[</keycap><keycap>]</keycap> keys with <keycap function=\"alt"
"\">Alt</keycap> perform slow pixel-size rotation."
msgstr ""
"또는 당신은 키보드에서 <keycap>[</keycap>그리고<keycap>]</keycap>(15도) 또는 "
"<keycap>Ctrl+[</keycap>그리고<keycap>Ctrl+]</keycap>(90도)를 눌러서 회전할 "
"수 있다. 동일한 <keycap>[]</keycap> 키와 <keycap>Alt </keycap>키를 사용하면 "
"픽셀 크기의 느린 회전을 수행합니다."

#: tutorial-tips.xml:296(title)
msgid "Drop shadows"
msgstr "그림자 드롭"

#: tutorial-tips.xml:297(para)
#, fuzzy
msgid ""
"To quickly create drop shadows for objects, use the "
"<menuchoice><guimenu>Filters</guimenu><guisubmenu>Shadows and Glows</"
"guisubmenu><guimenuitem>Drop Shadow</guimenuitem></menuchoice> feature."
msgstr ""
"오브젝트에 대한 그림자 드롭을 빠르게 생성하려면 <command>필터 &gt; 그림자 및 "
"광채 &gt; 그림자 드롭을 사용하십시오... 특징."

#: tutorial-tips.xml:301(para)
#, fuzzy
msgid ""
"You can also easily create blurred drop shadows for objects manually with "
"blur in the Fill and Stroke dialog. Select an object, duplicate it by "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>D</keycap></"
"keycombo>, press <keycap>PgDown</keycap> to put it beneath original object, "
"place it a little to the right and lower than original object. Now open Fill "
"And Stroke dialog and change Blur value to, say, 5.0. That's it!"
msgstr ""
"또한 당신은 채우기 및 스트로크 대화 상자에서 흐릿하게 표시하여 오브젝트의 흐"
"릿한 그림자 드롭을 수동으로 쉽게 만들 수 있습니다. 오브젝트를 선택하고 "
"<keycap>Ctrl+D</keycap>로 복제한 후 <keycap>PgDown</keycap>을 눌러 원본 오브"
"젝트 아래에 놓고 오른쪽으로 약간, 원본 오브젝트보다 낮게 배치하십시오. 이제 "
"채우기 및 스트로크 대화 상자를 열고 Blur 값을 5.0으로 변경하십시오. 그게 끝입"
"니다!"

#: tutorial-tips.xml:311(title)
msgid "Placing text on a path"
msgstr "경로에 텍스트 배치하기"

#: tutorial-tips.xml:312(para)
#, fuzzy
msgid ""
"To place text along a curve, select the text and the curve together and "
"choose <guimenuitem>Put on Path</guimenuitem> from the <guimenu>Text</"
"guimenu> menu. The text will start at the beginning of the path. In general "
"it is best to create an explicit path that you want the text to be fitted "
"to, rather than fitting it to some other drawing element — this will give "
"you more control without screwing over your drawing."
msgstr ""
"곡선을 따라 텍스트를 배치하려면 텍스트와 곡선을 함께 선택하고 텍스트 메뉴에"
"서 <command>경로 설정</command>을 선택하십시오. 텍스트는 경로의 시작 부분에"
"서 시작할 것입니다. 일반적으로 텍스트를 다른 그림 요소에 합치는 대신 텍스트"
"를 합칠 명시적 경로를 생성하는 것이 가장 좋습니다. 이렇게 하면 그림을 망치지 "
"않고도 더 많은 컨트롤을 할 수 있지요."

#: tutorial-tips.xml:322(title)
msgid "Selecting the original"
msgstr "원본 선택"

#: tutorial-tips.xml:323(para)
#, fuzzy
msgid ""
"When you have a text on path, a linked offset, or a clone, their source "
"object/path may be difficult to select because it may be directly "
"underneath, or made invisible and/or locked. The magic key <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> will help "
"you; select the text, linked offset, or clone, and press <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> to move "
"selection to the corresponding path, offset source, or clone original."
msgstr ""
"경로, 링크된 오프셋 또는 복제본에 텍스트가 있는 경우, 해당 텍스트가 바로 아래"
"에 있거나, 보이지 않거나, 잠겨 있어서 해당 소스 오브젝트/경로를 선택하기가 어"
"려울 수 있습니다. 마법 키 <keycap>Shift+D</keycap>를 사용하여 텍스트, 링크된 "
"오프셋 또는 복제를 선택하고 <keycap>Shift+D</keycap>를 눌러 선택 항목을 해당 "
"경로, 오프셋 소스 또는 복제본으로 이동하십시오."

#: tutorial-tips.xml:333(title)
#, fuzzy
msgid "Window off-screen recovery"
msgstr "윈도우 화면 밖 복구"

#: tutorial-tips.xml:334(para)
#, fuzzy
msgid ""
"When moving documents between systems with different resolutions or number "
"of displays, you may find Inkscape has saved a window position that places "
"the window out of reach on your screen. Simply maximise the window (which "
"will bring it back into view, use the task bar), save and reload. You can "
"avoid this altogether by unchecking the global option to save window "
"geometry (<guimenuitem>Inkscape Preferences</guimenuitem>, "
"<menuchoice><guimenu>Interface</guimenu><guimenuitem>Windows</guimenuitem></"
"menuchoice> section)."
msgstr ""
"해상도가 서로 다른 시스템 간에 문서를 이동할 때, 잉크스케이프가 윈도우를 화면"
"에서 손이 닿지 않는 위치에 저장하는 것을 발견할 수 있습니다. 창을 최대화하면"
"(다시 보기, 작업 표시줄 사용), 저장 및 다시 로드할 수 있습니다. 창 지오메트리"
"를 저장하기 위한 전역 옵션을 선택 취소하여 이 문제를 완전히 방지할 수 있습니"
"다(<command>Incscape Preferences</command>, <command>인터페이스 &gt; Windows "
"</command> 섹션)."

#: tutorial-tips.xml:345(title)
msgid "Transparency, gradients, and PostScript export"
msgstr "투명도, 그라데이션 및 PostScript 내보내기"

#: tutorial-tips.xml:346(para)
#, fuzzy
msgid ""
"PostScript or EPS formats do not support <emphasis>transparency</emphasis>, "
"so you should never use it if you are going to export to PS/EPS. In the case "
"of flat transparency which overlays flat color, it's easy to fix it: Select "
"one of the transparent objects; switch to the Dropper tool (<keycap>F7</"
"keycap> or <keycap>d</keycap>); make sure that the <guilabel>Opacity: Pick</"
"guilabel> button in the dropper tool's tool bar is deactivated; click on "
"that same object. That will pick the visible color and assign it back to the "
"object, but this time without transparency. Repeat for all transparent "
"objects. If your transparent object overlays several flat color areas, you "
"will need to break it correspondingly into pieces and apply this procedure "
"to each piece. Note that the dropper tool does not change the opacity value "
"of the object, but only the alpha value of its fill or stroke color, so make "
"sure that every object's opacity value is set to 100% before you start out."
msgstr ""
"PostScript 또는 EPS 형식은 <emphasis>투명성</emphasis>을 지원하지 않으므로 "
"PS/EPS로 내보내려면 절대 사용하지 마십시오. 평평한 색상에 겹쳐지는 평평한 투"
"명도의 경우 이 문제를 쉽게 해결할 수 있습니다: 투명 물체 중 하나를 선택하고 "
"Dropper 도구(<keycap>F7</keycap> or <keycap>d</keycap>)로 전환하십시오. 드로"
"퍼 도구의 도구 모음에서 <command>불투명도: 선택</command> 버튼이 비활성화된 "
"경우, 동일한 개체를 클릭하십시오. 그렇게 되면 눈에 보이는 색을 골라 다시 물체"
"에 할당하게 되지만, 이번에는 투명성이 없습니다. 모든 투명 개체에 대해 반복하"
"십시오. 투명 물체가 여러 개의 평평한 색 영역과 겹쳐진 경우, 당신은 그것을 적"
"절히 조각으로 부숴서 각 조각에 이 절차를 적용해야 할 것입니다. 드로퍼 도구는 "
"개체의 불투명도 값을 변경하지 않고 채우기 또는 스트로크 색상의 알파 값만 변경"
"하므로 시작하기 전에 모든 개체의 불투명도 값이 100%로 설정되어 있는지 확인하"
"십시오."

#: tutorial-tips.xml:363(title)
msgid "Interactivity"
msgstr ""

#: tutorial-tips.xml:364(para)
msgid ""
"Most SVG elements can be tweaked to react to user input (usually this will "
"only work if the SVG is displayed in a web browser)."
msgstr ""

#: tutorial-tips.xml:365(para)
msgid ""
"The simplest possibility is to add a clickable link to objects. For this "
"<mousebutton role=\"right-click\">right-click</mousebutton> the object and "
"select <menuchoice><guimenuitem>Create Link</guimenuitem></menuchoice> from "
"the context menu. The \"Object attributes\" dialog will open, where you can "
"set the target of the link using the value of <guilabel>href</guilabel>."
msgstr ""

#: tutorial-tips.xml:366(para)
msgid ""
"More control is possible using the interactivity attributes accessible from "
"the \"Object Properties\" dialog (<keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap function=\"shift\">Shift</keycap><keycap>O</keycap></"
"keycombo>). Here you can implement arbitrary functionality using JavaScript. "
"Some basic examples:"
msgstr ""

#: tutorial-tips.xml:369(para)
msgid "Open another file in the current window when clicking on the object:"
msgstr ""

#: tutorial-tips.xml:371(para)
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.location='file2.svg';</code>"
msgstr ""

#: tutorial-tips.xml:375(para)
msgid "Open an arbitrary weblink in new window when clicking on the object:"
msgstr ""

#: tutorial-tips.xml:377(para)
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.open(\"https://inkscape.org"
"\",\"_blank\");</code>"
msgstr ""

#: tutorial-tips.xml:381(para)
msgid "Reduce transparency of the object while hovering:"
msgstr ""

#: tutorial-tips.xml:383(para)
msgid ""
"Set <guilabel>onmouseover</guilabel> to <code>style.opacity = 0.5;</code>"
msgstr ""

#: tutorial-tips.xml:384(para)
msgid "Set <guilabel>onmouseout</guilabel> to <code>style.opacity = 1;</code>"
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tips.xml:0(None)
msgid "translator-credits"
msgstr "Heetae Hwang <heetae185@naver.com>, 2019"

#~ msgid "Click with the right mouse button"
#~ msgstr "마우스 오른쪽 버튼을 클릭하세요"
